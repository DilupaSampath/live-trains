const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  name: joi.string().required(),
  location_type:joi.string().required(),
  location: joi.object().keys({
    type:  joi.string().required(),
    coordinates: joi.array().items(
      joi.number().required()
  ),
  })
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  id: joi.string().alphanum().min(24).max(24),
  name: joi.string(),
  location_type:joi.string(),
  location: joi.object().keys({
    type:  joi.string().required(),
    coordinates: joi.array().items(
      joi.number()
        ),
  })
});