var insideCircle = require('geolocation-utils');
var moment = require('moment');
moment().format();
var request = require('request');
const geolib = require('geolib');
const trainService = require('../src/train/trainService');
const titmeTableService = require('../src/time-table/timeTableService');
const distance = require('google-distance-matrix');
distance.key('AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8');
var GOOGLE_NEAR_BY_PLACE_API_URL = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?';
var GOOGLE_NEAR_BY_PLACE_API_URL_TEXT_SEARCH = 'https://maps.googleapis.com/maps/api/place/textsearch/json?';
var GOOGLE_DIRECTION_API_URL = 'https://maps.googleapis.com/maps/api/directions/json?';
//https://maps.googleapis.com/maps/api/directions/json?origin=6.937432,79.879112&destination=6.929831,79.861096&mode=transit&transit_mode=train&transit_routing_preference=less_walking&key=AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8

async function getDistance(data) {
    return new Promise(async (resolve, reject) => {
        console.log("***********8origin");
var origin={};
var destination ={};
makeNearByPlaceRequest({"lat":data.originLatAndLon.split(",")[0],"lang":data.originLatAndLon.split(",")[1]},async function (err, nearByPlaces) {
            if (!err){
                origin=nearByPlaces;
                console.log(origin);
                makeNearByPlaceRequest({"lat":data.destinationsLatAndLon.split(",")[0],"lang":data.destinationsLatAndLon.split(",")[1]},async function (err, nearByPlaces) {
                    if (!err){
                        destination=nearByPlaces;
                   
// console.log(origin);
var exactOriginCordinates =await filterTrainStationsCordinates(origin);
var exactDestinationCordinates =await filterTrainStationsCordinates(destination);


if(exactOriginCordinates !=0 &&  exactDestinationCordinates !=0){

}else{
    return resolve("No direct train found");
}
        var origins = [exactOriginCordinates.cordinates.lat+","+exactOriginCordinates.cordinates.lng];
        var destinations = [exactDestinationCordinates.cordinates.lat+","+exactDestinationCordinates.cordinates.lng];
 
        var result;
        distance.transit_routing_preference('less_walking');
        distance.mode('transit');
        distance.transit_mode('rail');
        distance.matrix(origins, destinations, function (err, distances) {
           
            if (!err){
               
                result = distances;
                return resolve(result);
            }else{
 
                return reject(err) ;
            }
        });
                    }else{
                        destination={};
                    }
                        
                });
            }else{
                origin={};
            }
                
        } );



       
    });
    

}
async function getSpeed(data){
    var speed = await geolib.getSpeed(
       
        { latitude: data.firstLat, longitude: data.firstLng, time:data.firstTime },
        { latitude: data.secondLat, longitude: data.secondLng, time:data.secondTime  }
    );
    return speed;
}

//broadcast pump data
function filterRunningSchedules(trainData) {
var data = [];
data =trainData;
var result = data.filter(schedule => schedule.isRunung==true);
return result;
}
function filterTrainStationsCordinates(nearByLocationData) {
    return new Promise((resolve, reject) => {
    try {
    var data = [];
    data =nearByLocationData.results;
    var result = data.filter(item => item.types.includes("train_station")==true);
    if(result.length>0){
    
        return resolve({"name":result[0].name,"cordinates":result[0].geometry.location});
    }else{
        return resolve(0);
    }
   
    } catch (error) {
        return reject(1);
    }
});
    }

    function filterTrainStations(nearByLocationData) {
        return new Promise((resolve, reject) => {
        try {
        var data = [];
        data =nearByLocationData.results;
        var result = data.filter(item => item.types.includes("train_station")==true);
        
        if(result.length>0){
            var res=[];
            result.forEach(element => {
      res.push({
        "lat": element.geometry.location.lat,
        "lng":element.geometry.location.lng,
        "draggable": true
      });
            });
            return resolve(res);
        }else{
            return resolve(0);
        }
       
        } catch (error) {
            return reject(1);
        }
    });
        }
    async function  getDataWithArrivelTime (data) {
    var speed = await geolib.getSpeed(
        { latitude: data.firstLat, longitude: data.firstLng, time:data.firstTime },
        { latitude: data.secondLat, longitude: data.secondLng, time:data.secondTime  }
    );
    
    // var trainData =await trainService.findOne({name:'Test train XXX'});
    var timeTableData =await titmeTableService.findTimeTable({name:'Maradana To Magalegoda Schedule'});

    var distan = geolib.getDistance(
    {latitude: data.secondLat, longitude: data.secondLng}, 
    { latitude: timeTableData.end.location.coordinates[1], longitude: timeTableData.end.location.coordinates[0]}
    );

   
var lastDis = geolib.convertDistance(distan, 'km');
var lastSpee = geolib.convertSpeed(speed, 'kmh');

// var timeTableData =await titmeTableService.findOne(data.timetableId);
// var timeTableDataRes =await titmeTableService.findByIdAndUpdate(data.timetableId,{
//     end_time: new Date((new Date().getTime())) + new Date(parseInt(newtimeTableData.end_time)).getTime()  
// });
console.log("************************");
console.log(timeTableData._id);
console.log("************************");
var t = await titmeTableService.findByIdAndUpdate(timeTableData._id,
    {
        short_end_time:moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60)))).format("YYYY-MM-DD | HH:mm:ss")
    });
var res = {
firstTime: moment(new Date(data.firstTime)).format("YYYY-MM-DD | HH:mm:ss"),
secondTime: moment(new Date(data.secondTime)).format("YYYY-MM-DD | HH:mm:ss"),
systemTime: moment(new Date()).format("YYYY-MM-DD | HH:mm:ss"),
trainName:timeTableData.train.name,
startStation:timeTableData.start,
endStation:timeTableData.end,
toDiurationInMinutes:((distan/speed)/60),
toDiurationInSec:((distan/speed)),
toDistanceInMeters:distan,
arrvalTime:moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60)))).format("YYYY-MM-DD | HH:mm:ss"),
speedMetersPerSecond:speed,
speedMetersPerMinute:speed*60
}

return res;
    }

 function getRealLocationData(data){
        return new Promise(async (resolve, reject) => {
    var responseObject={};
    var location ={};
                    // console.log(origin);
                    makeNearByPlaceRequest({"lat":data.lat,"lang":data.lang},async function (err, nearByPlaces) {
                        if (!err){
                            location=nearByPlaces;
    var exactLocationCordinates =await filterTrainStationsCordinates(location);
    responseObject.status=true;
    responseObject.data=exactLocationCordinates;
    return resolve(responseObject);
}else{
    responseObject.status=false;
    return  reject(responseObject)
    }
    });
        });
    }


function makeNearByPlaceRequest(optionsDetails, callback) {
   var options = {
        location: optionsDetails.lat+'%2C'+optionsDetails.lang,
        rankby: 'distance',
        type: 'train_station',
        key: 'AIzaSyAfasOqXrX8t2kNPaTVcTJoXOPTVjkcnXY'
      }
    var requestURL = GOOGLE_NEAR_BY_PLACE_API_URL_TEXT_SEARCH + 'location='+optionsDetails.lat+'%2C'+optionsDetails.lang+'&rankby=distance&type=train_station&key=AIzaSyAfasOqXrX8t2kNPaTVcTJoXOPTVjkcnXY';

    request(requestURL, function(err, response, data) {
      if (err || response.statusCode != 200) {
        return callback(new Error('Google API request error: ' + data));
      }
      var res =filterTrainStations(JSON.parse(data))
      callback(null, JSON.parse(data));
    })
  }
 async function makeNearByPlaceRequestTrainFilter(optionsDetails, callback) {
    var options = {
         location: optionsDetails.lat+'%2C'+optionsDetails.lang,
         rankby: 'distance',
         type: 'train_station',
         key: 'AIzaSyAfasOqXrX8t2kNPaTVcTJoXOPTVjkcnXY'
       }
     var requestURL = GOOGLE_NEAR_BY_PLACE_API_URL_TEXT_SEARCH + 'location='+optionsDetails.lat+'%2C'+optionsDetails.lang+'&rankby=distance&type=train_station&key=AIzaSyAfasOqXrX8t2kNPaTVcTJoXOPTVjkcnXY';
 
     request(requestURL,async function(err, response, data) {
       if (err || response.statusCode != 200) {
         return callback(new Error('Google API request error: ' + data));
       }
       var res =await filterTrainStations(JSON.parse(data));
       callback(null,res);
     })
   }
//broadcast pump data
// function getRealLocationData(data){
    function checkPointIsOnThePath (point, data)  {
    return new Promise(async (resolve, reject) => {
        try {
            // let item = await trainService.findOne(req.body.id);
      
//             data.forEach(element => {
// var o = JSON.stringify(element);                
// var g = JSON.parse(o);                
//                 console.log(g.coordinates);
//     });
            for (var i = 0; i < data.length; i++) {
                // console.log(data[i]);
                var o = JSON.stringify(data[i]);                
                var g = JSON.parse(o);  

            //   var isInPath =   geolib.isPointWithinRadius(
            //         point,
            //         { latitude: g.coordinates[1], longitude: g.coordinates[0] },
            //         50
            //     );
            var isInPath = insideCircle.insideCircle(point, {lat: g.coordinates[1], lon: g.coordinates[0]}, 30) 
            
                if (isInPath) {
                    return  resolve(true);
                }
                // if(data.length==i){
              
                // }
              }
              return resolve(false);
    
            // response.successWithData(item, res)
        } catch (error) {
            // response.customError('' + error, res)
            return reject(error);
        }
    });
    
}



// function  checkPointIsOnThePath (point,data) {

//     for (var i = 0; i < data.path_cordinates.length; i++) {
//         console.log(data.path_cordinates); // result: "My","name"
//       var isInPath =   geolib.isPointWithinRadius(
//             point,
//             data.path_cordinates[i],
//             500
//         );
//         console.log(isInPath); // result: "My","name"

//         if (isInPath) {
//             return true;
//         }
//       }
//       return false;
//     }
module.exports = {
    getDistance,
    getDataWithArrivelTime,
    makeNearByPlaceRequest,
    getRealLocationData,
    checkPointIsOnThePath,
    makeNearByPlaceRequestTrainFilter,
    getSpeed
};