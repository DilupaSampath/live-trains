import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { Router } from '@angular/router';

// import service
import { AuthService } from '../../services/authentication.service';
import { MsgHandelService } from '../../services/msg-handel.service';
import { JwtTokenValidatorService } from '../../services/jwt-token-validator.service';

declare const $: any;
import { environment } from '../../../../environments/environment';
import swal from 'sweetalert2';

// import route config
import { ADMIN_ROUTES, AGENT_ROUTES } from '../../../../assets/js/route-config';

@Component({
  selector: 'app-sidebar-cmp',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public menuItems: any[];

  // user details
  username:"ADMIN";
  photo: String = 'assets/img/default-avatar.png';
  is_admin: Boolean = false;
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  constructor(
    private _Router: Router,
    private _AuthService: AuthService,
    private _MsgHandelService: MsgHandelService,
    private _JwtTokenValidatorService: JwtTokenValidatorService
  ) { }

  ngOnInit() {
    this.is_admin = true;
    this.menuItems = ADMIN_ROUTES
    // if (this._JwtTokenValidatorService.validateUserRole('admin')) {
    //   this.is_admin = true;
    //   this.menuItems = ADMIN_ROUTES.filter(menuItem => menuItem);
    // } else if (this._JwtTokenValidatorService.validateUserRole('agent')) {
    //   this.showImage();
    //   this.is_admin = false;
    //   this.menuItems = AGENT_ROUTES.filter(menuItem => menuItem);
    // }

    // set user email as the user name
    // this.username = this._LocalStorageHandleService.getItem('email');
  }

  // showImage() {
  //   this._AuthService
  //     .getSingleUser(this._JwtTokenValidatorService.accessUser_Id())
  //     .subscribe(
  //       response => {
  //         if (this._MsgHandelService.handleSuccessResponse(response)) {
  //           const apiData = response['data']['value'];

  //           if (
  //             apiData.photo !== undefined &&
  //             apiData.photo !== null &&
  //             !this.is_admin
  //           ) {
  //             const image_name = apiData['photo']['file_name'];
  //             this.photo = `${
  //               environment.imageUploadPath
  //               }/300x300/${image_name}`;
  //           }
  //         }
  //       },
  //       error => {
  //         this._MsgHandelService.handleError(error);
  //       }
  //     );
  // }

  updatePS(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>(
        document.querySelector('.sidebar .sidebar-wrapper')
      );
      const ps = new PerfectScrollbar(elemSidebar, {
        wheelSpeed: 2,
        suppressScrollX: true
      });
    }
  }
  isMac(): boolean {
    let bool = false;
    if (
      navigator.platform.toUpperCase().indexOf('MAC') >= 0 ||
      navigator.platform.toUpperCase().indexOf('IPAD') >= 0
    ) {
      bool = true;
    }
    return bool;
  }

  navigateToProfile() {
    this._Router.navigate(['/user-profile'], {
      queryParams: {
        operation: 'edit_profile',
        userId: this._JwtTokenValidatorService.getLoggedUserId()
      }
    });
  }

  logOut(): void {
    swal({
      title: 'Are you sure?',
      text: 'Are you sure that you want to Log out?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes',
      buttonsStyling: false
    }).then(willDelete => {
      if (willDelete.value) {
        window.localStorage.clear();
        this._MsgHandelService.showSuccessMsg(
          'Signed out!',
          'successfully logout'
        );
        this._Router.navigateByUrl('/login');
        setTimeout(() => {
          location.reload();
        }, 2000);
      }
    });
  }

}
