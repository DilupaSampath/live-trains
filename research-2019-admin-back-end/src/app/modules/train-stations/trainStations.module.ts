import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { AppMaterialModule } from "../../app.material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { _Routes } from "./trainStations.routing";

/* services */
import { InitService } from "./services/init/init.service";

/* components */
import { InitComponent } from "./init/init.component";
import { FilterPipe } from "./filter.pipe";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppMaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild(_Routes)
  ],
  declarations: [InitComponent, FilterPipe],
  providers: [InitService]
})
export class TrainStationsModule {}
