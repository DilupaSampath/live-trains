const cors = require('cors');
const mongoose = require('mongoose');
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
const bodyParser = require('body-parser');

const express = require('express');
const http = require('http');

const config = require('./config/config');
const routes = require('./routes/routes');
const cronService = require('./services/cronServices');

const server = express();
const server_port = config.web_port;

const session = require('express-session') // -> session storage using mongo
const path = require('path')

server.use(session({ secret: 'zz', resave: true, saveUninitialized: true }))

server.use(bodyParser.json({limit: '50mb'}));
server.use(cors());
server.use(routes);
server.use(express.static(__dirname));
server.use(express.static(path.join(__dirname, './dist/'))) // BUT ON PRODUCTION -> nginx
server.use(express.json({limit: '50mb'}));
server.use(express.urlencoded({limit: '50mb'}));


mongoose.connect(config.databaseUrl, { useNewUrlParser: true });
// mongoose.set('debug', true);

// Create socket server
var httpServer = http.createServer(server);
const io = require('./services/socketService').listen(httpServer);
cronService.runCheckMainValvesStatusCron();
cronService.runReadMainValveDataCron();
// Start socket server
httpServer.listen(server_port, err => {
    if (err) {
        console.error(err);
    }
    else {
        console.log('server listening on port: ' + server_port);
    }
});

module.exports = httpServer;