const trainModel = require('./trainModel');
const geolib = require('geolib');
var ObjectID = require('mongodb').ObjectID; 
/**
 * find from the system
 */
// module.exports.findLocations = (condition) => {
//     return new Promise((resolve, reject) => {
//         trainModel.find(condition).exec((err, trains) => {
//             err ? reject(err) : resolve(trains);
//         });
//     })
// };

module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        trainModel.find(condition).exec((err, trains) => {
            err ? reject(err) : resolve(trains);
        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        trainModel.deleteOne({"_id":id},
             (err, train) => {
                if (err)
                    reject(err);
                else if (train != null || train != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let item = new trainModel();
            item.name = data.name;
            item.isRunning = data.isRunning;
            item.current_location = data.current_location;
            item.selectedPoints = data.selectedPoints;
            item.save(async (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(data);
                }
            })
        } catch (error) {
            reject('' + error)
        }
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        
        trainModel.findOne({ _id: new ObjectID(id) })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = (id, data) => {
    return new Promise((resolve, reject) => {
        trainModel.findByIdAndUpdate(id, data, {safe:true})
            .exec((err, result) => {
                if (err)
                    reject(err)
                else if (result == null || result == undefined)
                    reject("Invalid id");
                else
                    resolve(result);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findTrainIdAccordingToLocation =async (data) => {
    return new Promise( async (resolve, reject) => {
    let count=0;
      let trainData =await this.getAll({});
      trainData.forEach(element => {
          element.selectedPoints.forEach(location => {
            

           let status =  geolib.isPointWithinRadius(
                { latitude: data.latitude, longitude:data.longitude },
                { latitude:location.latitude, longitude: location.longitude },
                50
            );
            count++;
            if(status==true){
return {status:true,trainId:element._id};
            }else{
                if(trainData.length==count){
                    return {status:false,trainId:null};
                }
            }
        });
      });
    })
};