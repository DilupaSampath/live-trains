/* Built-in Modules */
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/**
 * Custom Modules
 */
import { ToastrModule } from 'ngx-toastr';
import { AppRoutes } from './app.routing';
import { AppMaterialModule } from './app.material.module';
import { LightboxModule } from 'ngx-lightbox'; // import light box
import { FooterModule } from './commons/components/footer/footer.module';
import { NavbarModule } from './commons/components/navbar/navbar.module';
import { SidebarModule } from './commons/components/sidebar/sidebar.module';
import { ImageUploadModule } from 'angular2-image-upload'; // image upload library

/**
 * common services
 */
import { JwtService } from './infrastructure/jwt.service';
import { MainService } from './infrastructure/api.service';
import { AuthService } from './commons/services/authentication.service';
import { AuthGuardService } from './infrastructure/auth-guard.service';
import { MsgHandelService } from './commons/services/msg-handel.service';
import { JwtTokenValidatorService } from './commons/services/jwt-token-validator.service';
import { AuthRouteGuardService } from './commons/services/authorization-route-guard.service';

/**
 * common Components
 */
import { AppComponent } from './app.component';
import { LoginComponent } from './commons/components/login/login.component';
import { AdminLayoutComponent } from './commons/layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './commons/layouts/auth/auth-layout.component';
import { RegisterComponent } from './commons/components/register/register.component';
import { DateDifferencePipe } from './commons/pipes/date-difference.pipe';
import { OrderByPipe } from './commons/pipes/order-by.pipe';
import { YearMonthPipe } from './commons/pipes/year-month.pipe';
import { FilterPipe } from './filter.pipe';
@NgModule({
  imports: [
    HttpModule,
    FormsModule,
    NavbarModule,
    FooterModule,
    CommonModule,
    SidebarModule,
    LightboxModule,
    HttpClientModule,
    AppMaterialModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ImageUploadModule.forRoot(),
    RouterModule.forRoot(AppRoutes)
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AuthLayoutComponent,
    AdminLayoutComponent,
    DateDifferencePipe,
    OrderByPipe,
    YearMonthPipe,
    FilterPipe
  ],
  providers: [
    JwtService,
    MainService,
    AuthService,
    MsgHandelService,
    AuthGuardService,
    AuthRouteGuardService,
    JwtTokenValidatorService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
