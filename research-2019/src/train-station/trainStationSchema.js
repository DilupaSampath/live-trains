const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  useLocationName: joi.boolean(),
  name: joi.string().required(),
  location: joi.string().alphanum().min(24).max(24)
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  id:joi.string().alphanum().min(24).max(24).required(),
  name: joi.string(),
  location: joi.string().alphanum().min(24).max(24)
});