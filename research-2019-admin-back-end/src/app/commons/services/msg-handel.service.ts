import { Injectable } from '@angular/core';
import { ErrorHandler } from '@angular/core';

/**
 * services import
 */
import { JwtService } from '../../infrastructure/jwt.service';

/**
 * HTTP errors
 */
import { UNAUTHORIZED, BAD_REQUEST, FORBIDDEN } from 'http-status-codes';

/**
 * common services
 */
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MsgHandelService implements ErrorHandler {
  // declare error msg
  private REFRESH_MESSAGE = 'Something went wrong. Please Login again!';
  private DEFAULT_ERROR_TITLE = 'Something went wrong';

  constructor(
    private _Router: Router,
    private _JwtService: JwtService,
    private _ToastrService: ToastrService
  ) { }

  /**
   *
   * @param response : response getting from the api call
   */
  public handleSuccessResponse(response: any): Boolean {
    console.log(response['error']);
    console.log(response['data']);
    if (response['status'] ===true) {
      if (response['data'] === undefined) {
        this._ToastrService.error(
          'UnSupported Data Format From Server!',
          'Error'
        );
        return false;
      } else {
        if (response['data'] !== undefined) {
          return true;
        } else {
          return false;
        }
      }
    } else {
      this._ToastrService.error(response['error'].message, 'Error');
      return false;
    }
  }

  /**
   * show success msg as notifications
   * @param title : string
   * @param content : string
   */
  public showSuccessMsg(title: string, content: string) {
    this._ToastrService.success(content, title);
  }

  /**
   * show success msg as notifications
   * @param title : string
   * @param content : string
   */
  public showWarningMsg(title: string, content: string) {
    this._ToastrService.warning(content, title);
  }

  /**
   * show Error msg as notifications
   * @param title : string
   * @param content : string
   */
  public showErrorMsg(title: string, content: string) {
    this._ToastrService.error(content, title);
  }

  /**
   * handle common errors
   * @param error : error with the response
   */
  public handleError(error: any) {
    let errorMsg = '';
    let errorTitle = 'Connection Issue';

    const httpErrorCode = error.httpErrorCode;
    console.log(error);
if(error.msg.includes("already")){
  errorTitle = error.msg;  
  this.showError(errorTitle, errorTitle);
}else{
  if (error['error'] !== undefined && error['error'] !== null) {
    if (error['error']['statusCode'] === 401) {
      errorMsg = 'You Unauthorized to proceed this action !';
    } else if (error['error']['statusCode'] === 400) {
      errorMsg = 'Request data incomplete !';
    } else {
      errorMsg = error['error']['message'];
    }

    errorTitle = error['error']['name'];
  }
  this.showError(errorTitle, errorMsg);
}
    
    // call for customized error handle
 
  }

  /**
   * show error msg in common way
   * @param message : error msg
   */
  private showError(title: string, message: string) {
    this._ToastrService.error(message, title);
  }

}
