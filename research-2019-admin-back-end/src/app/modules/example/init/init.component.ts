import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { InitService } from '../services/init/init.service';
import { MsgHandelService } from 'src/app/commons/services/msg-handel.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { MatSlideToggleChange } from '@angular/material';
@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {
  searchText:any;
allLocations:any;
location:any;
searchStatus:any="Create Location";
status:boolean=false;
@Input() adressType: string;
@Output() setAddress: EventEmitter<any> = new EventEmitter();
@ViewChild('addresstext') addresstext: any;

autocompleteInput: string;
queryWait: boolean;


  constructor(    private _Router: Router, private _InitService: InitService, private _MsgHandelService: MsgHandelService) { }
  ngAfterViewInit() {
    this.getPlaceAutocomplete();
}
private getPlaceAutocomplete() {
  const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
          componentRestrictions: { country: 'LK' },
          types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
      });
  google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);
  });
}
 toggle(event: MatSlideToggleChange) {
  console.log('toggle', event.checked);
  if(event.checked){
    this.status=true;
    this.searchStatus="Search Location";
  }else{
    this.status=false;
    this.searchStatus="Create Location";

  }
}
getAddress(place: object) { 
  
  console.log(place['formatted_address']);
}
invokeEvent(place: any) {
  this.location={
    "name":place.adr_address,
    "location_type":"train",
    "location":{
      "type":"Point",
      "coordinates":[place.geometry.location.lat(),place.geometry.location.lng()]
    }
  }
  console.log(this.location);
  
  this.setAddress.emit(place);
}

  ngOnInit() {
    this.getAllLocationData();
  }
  getAllLocationData(){
    this._InitService.getAll().subscribe(
      Response => {
        if (this._MsgHandelService.handleSuccessResponse(Response)) {
          this.allLocations = Response['data'];
          console.log(" this.allLocations");
          console.log( this.allLocations);
        }
      },
      error => {
        this._MsgHandelService.handleError(error);
      }
    );
  }
  createLocation(){
 
    swal({
      title: 'Are you sure?',
      text: 'Do you need save this location!',
      type: 'info',
      showCancelButton: true,
      showLoaderOnConfirm: true
    }).then(result => {
      if (result.value) {
        this._InitService.postObj(this.location).subscribe(
          locationResponse => {
            if (this._MsgHandelService.handleSuccessResponse(locationResponse)) {
              this._MsgHandelService.showSuccessMsg(
                '',
                'Successfully saved the location!'
              );
              // this._Router.navigateByUrl('/locations');
              this.getAllLocationData();
            }
          },
          error => {
            this._MsgHandelService.handleError(error);
          }
        );
      }
    });
    
  }
  deleteLocation(location:any){
    let obj={"id":location._id};
    swal({
      title: 'Are you sure?',
      text: 'Do you need to delete this location!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes',
      buttonsStyling: false
    }).then(result => {
      if (result.value) {
        this._InitService.deleteObj(obj).subscribe(
          data => {
            if (this._MsgHandelService.handleSuccessResponse(data)) {
              this._MsgHandelService.showSuccessMsg(
                '',
                'Location successfully deleted !'
              );
              this.getAllLocationData();
            }
          },
          err => {
            this._MsgHandelService.handleError(err);
          }
        );
      }
    });
  
  }
}
