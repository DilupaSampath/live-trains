
const dateService = require('./dateService');
const CronJob = require('cron').CronJob;
const socketService = require('./socketService');

//pump cron service
function runCheckPumpStatusCron() {
    new CronJob('*/20 * * * * *', async () => {
    }, null, true, '');
}

//main valve cron service
function runCheckMainValvesStatusCron() {
    new CronJob('*/2 * * * * *', async () => {
        console.log("rning");
        socketService.emitMainValveData({
            data: "table data"
        });
    }, null, true, '');
}

//main valve cron service
function runReadMainValveDataCron() {
    new CronJob('*/2 * * * * *', async () => {
        console.log("rning read");
        socketService.emitMainValveData({
            data: "table data"
        });
    }, null, true, '');
}

//rack cron service
function runCheckRacksStatusCron() {
    new CronJob('*/20 * * * * *', async () => {
    }, null, true, '');
}

//Run Server Time Cron cron service
function runServerTimeCron() {
    new CronJob('* * * * * *', async () => {
    }, null, true, '')
}
//export functions
module.exports = {
    runCheckMainValvesStatusCron,
    runReadMainValveDataCron
};