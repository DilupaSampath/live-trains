const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const timeTableSchema = new Schema(
    { 
        name: {
            type: String,
            required: true,
            unique: true
        },
        current_location: {
            type: { type: String },
            coordinates: []
           },
        train:{
            type: Schema.Types.ObjectId,
            ref: "trains"
        },
        start:{
            type: Schema.Types.ObjectId,
            ref: "locations"
        },
        end:{
            type: Schema.Types.ObjectId,
            ref: "locations"
        },
        distance:{
            type: String
        },
        isRunning: {
            type: Boolean,
            default: false
        },
        start_time: {
            type: Number,
            default: 0
        },
        end_time: {
            type: Number,
            default: 0
        },
        short_start_time: {
            type: String
        },
        short_end_time: {
            type: String
        }
    }, { timestamps: true }
);
timeTableSchema.index({ current_location: "2dsphere" });
module.exports = mongoose.model('timeTables', timeTableSchema);