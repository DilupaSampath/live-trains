import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

/**
 * import services
 */
import { MainService } from '../../infrastructure/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private _MainService: MainService) {}

  // user photo broadcaster
  private photo = new BehaviorSubject<string>('');
  public photoCast = this.photo.asObservable();

  // user name broadcaster
  private name = new BehaviorSubject<string>('');
  public nameCast = this.name.asObservable();

  updatePhoto(newPhoto) {
    this.photo.next(newPhoto);
  }

  updateName(newName) {
    this.name.next(newName);
  }

  /**
   * calling to login endpoint
   * @param user : userObject<email,password>
   */
  loginUser(user: any): Observable<any> {
    return this._MainService.post('user/login', user).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  /**
   * calling to forget password
   * @param user : userObject<email>
   */
  forgerPassword(user: any): Observable<any> {
    return this._MainService.post('password/forget', user).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  /**
   * Register User
   * @param user : userObject
   */
  registerUser(user): Observable<any> {
    return this._MainService.post('user/new', user).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  /**
   * Get Single User
   * @param user : userObject
   */
  getSingleUser(id): Observable<any> {
    return this._MainService.get(`users/getOne/${id}`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }
}
