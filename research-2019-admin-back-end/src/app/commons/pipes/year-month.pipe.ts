import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yearMonth'
})
export class YearMonthPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value !== undefined && value !== null) {
      return `${new Date(value).getFullYear()}-${(
        '0' +
        (new Date(value).getMonth() + 1)
      ).slice(-2)}`;
    } else {
      return null;
    }
  }
}
