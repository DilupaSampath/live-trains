const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  name: joi.string().required(),
  isRunning: joi.boolean(),
  current_location:joi.object().keys({
    type:  joi.string().required(),
    coordinates: joi.array().items(
      joi.number().required()
  ),
  }),
  train_stations:joi.array().items(
    joi.string().required()
).required(),
selectedPoints:joi.array().items(
  joi.object().keys({
    latitude:joi.number().required(),
    longitude:joi.number().required(),
  })
)
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  id:joi.string().alphanum().min(24).max(24).required(),
  name: joi.string(),
  isRunning: joi.boolean(),
  current_location:joi.object().keys({
    type:  joi.string().required(),
    coordinates: joi.array().items(
      joi.number().required()
  ),
  }),
  train_stations:joi.array().items(
    joi.string().required()
),
selectedPoints:joi.array().items(
  joi.object().keys({
    latitude:joi.number().required(),
    longitude:joi.number().required(),
  })
)
});