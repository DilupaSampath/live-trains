const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const trainSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true
        },
        isRunning:{
            type: Boolean,
            default: false
        },
        current_location: {
            type: { type: String },
            coordinates: []
           },
        train_stations:[{type:String,required:true}],
        selectedPoints:[ 
            {
                latitude:{type:Number,required:false},
                longitude:{type:Number,required:false},
            }
        ]

    }, { timestamps: true }
);
trainSchema.index({ current_location: "2dsphere" });
module.exports = mongoose.model('trains', trainSchema);