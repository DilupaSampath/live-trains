import { Component, OnInit } from '@angular/core';

/**
 * import service
 */
import { JwtService } from '../../../infrastructure/jwt.service';

@Component({
  selector: 'app-footer-cmp',
  templateUrl: 'footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  test: Date = new Date();

  loginStatus: Boolean = false;
  constructor(private _JwtService: JwtService) {
    this.loginStatus = false;
  }
  ngOnInit() {
    if (this._JwtService.getToken()) {
      this.loginStatus = true;
    } else {
      this.loginStatus = false;
    }
  }
}
