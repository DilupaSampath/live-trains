const timeTableService = require('./timeTableService');
const commonService = require('../../services/commonServices');
const response = require('../../services/responseService');

/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        let items = await timeTableService.getAll({})
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}


/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        let item = await timeTableService.findOne(req.body.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get one item
 */
module.exports.findOneByCondition = async (req, res) => {
    try {
        let item = await timeTableService.findTimeTable(req.body)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await timeTableService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        delete details.id;
        let item = await timeTableService
            .findByIdAndUpdate(req.body.id, details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        let item = await timeTableService.delete(req.body.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
