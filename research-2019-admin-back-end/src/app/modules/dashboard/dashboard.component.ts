/// <reference types="@types/googlemaps" />
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  NgZone,
  ElementRef
} from '@angular/core';
import { Router } from '@angular/router';
import { TableData } from './md/md-table/md-table.component';

// import e charts
import { EChartOption } from 'echarts';

/**
 * Import Services
 */
import { MapsAPILoader } from '@agm/core';
import { InitService } from './services/init.service';
import { MsgHandelService } from '../../commons/services/msg-handel.service';
import { JwtTokenValidatorService } from '../../commons/services/jwt-token-validator.service';

/**
 * JSON files
 */
import { mapStyles } from '../../../assets/ts/mapStyleConfig';

import * as Chartist from 'chartist';
declare const $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  // variables
  panelOpenState = false;
  lat: any = 7.8731;
  lng: any = 80.7718;
  zoom: Number = 7.4;
  saleStoreArray: Array<any> = [];
  isAdmin: Boolean = this._JwtTokenValidatorService.validateUserRole('admin');
  styles: any;
  mani_store_mapIcon: any = {
    url: 'assets/img/main_sale.svg',
    scaledSize: {
      width: 35,
      height: 55
    }
  };

  store_mapIcon: any = {
    url: 'assets/img/sale.svg',
    scaledSize: {
      width: 30,
      height: 50
    }
  };

  @ViewChild('search')
  public searchElementRef: ElementRef;
  trendingVehicle: any;
  trendingBrand: any;
  trendingDealer: any;
  unRegVehicleCount: any = 0;
  dataDailySalesChart: any;
  dataWebsiteViewsChart: any;

  popularData: any = {
    labels: [],
    series: [],
    icons: []
  };

  chartOption1: EChartOption = {};

  chartOption2: EChartOption = {};

  chartOption3: EChartOption = {};

  chartOption4: EChartOption = {};

  constructor(
    private _NgZone: NgZone,
    private _InitService: InitService,
    private _Router: Router,
    private _MapsAPILoader: MapsAPILoader,
    private _MsgHandelService: MsgHandelService,
    private _JwtTokenValidatorService: JwtTokenValidatorService
  ) {
    // this.loadStyles();
  }
  public tableData: TableData;
  startAnimationForLineChart(chart: any) {
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;
    chart.on('draw', function (data: any) {
      if (data.type === 'line' || data.type === 'area') {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path
              .clone()
              .scale(1, 0)
              .translate(0, data.chartRect.height())
              .stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
          }
        });
      } else if (data.type === 'point') {
        seq++;
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq = 0;
  }

  // viewSaleInfo(id: string) {
  //   this._Router.navigate(['/sales/summary'], {
  //     queryParams: {
  //       saleId: id
  //     }
  //   });
  // }

  // constructor(private navbarTitleService: NavbarTitleService) { }
  public ngOnInit() {
    // this.autoCompleteMap();

    /* ----------==========     Daily Sales Chart initialization    ==========---------- */

    this.dataDailySalesChart = {
      labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
      series: [[0, 0, 0, 0, 0, 0, 0]]
    };

    // this.loadData();
  }
  ngAfterViewInit() {
    const breakCards = true;
    if (breakCards === true) {
      // We break the cards headers if there is too much stress on them :-)
      $('[data-header-animation="true"]').each(function () {
        const $fix_button = $(this);
        const $card = $(this).parent('.card');
        $card.find('.fix-broken-card').click(function () {
          const $header = $(this)
            .parent()
            .parent()
            .siblings('.card-header, .card-image');
          $header.removeClass('hinge').addClass('fadeInDown');

          $card.attr('data-count', 0);

          setTimeout(function () {
            $header.removeClass('fadeInDown animate');
          }, 480);
        });

        $card.mouseenter(function () {
          const $this = $(this);
          const hover_count = parseInt($this.attr('data-count'), 10) + 1 || 0;
          $this.attr('data-count', hover_count);
          if (hover_count >= 20) {
            $(this)
              .children('.card-header, .card-image')
              .addClass('hinge animated');
          }
        });
      });
    }
  }

  // private loadData() {
  //   // get trending data
  //   // vehicles
  //   this._InitService.getTrendingData('vehicle').subscribe(
  //     response => {
  //       if (this._MsgHandelService.handleSuccessResponse(response)) {
  //         const data = response['data']['value'];
  //         this.trendingVehicle = data['value'];
  //       }
  //     },
  //     error => {
  //       this._MsgHandelService.handleError(error);
  //     }
  //   );

  //   // brand
  //   this._InitService.getTrendingData('brand').subscribe(
  //     response => {
  //       if (this._MsgHandelService.handleSuccessResponse(response)) {
  //         const data = response['data']['value'];
  //         this.trendingBrand = data['value'];
  //       }
  //     },
  //     error => {
  //       this._MsgHandelService.handleError(error);
  //     }
  //   );
  //   // dealer
  //   this._InitService.getTrendingData('dealer').subscribe(
  //     response => {
  //       if (this._MsgHandelService.handleSuccessResponse(response)) {
  //         const data = response['data']['value'];
  //         this.trendingDealer = data['value'];
  //       }
  //     },
  //     error => {
  //       this._MsgHandelService.handleError(error);
  //     }
  //   );

  //   // get popular brands
  //   // dealer
  //   this._InitService.getPopularBrands().subscribe(
  //     response => {
  //       if (this._MsgHandelService.handleSuccessResponse(response)) {
  //         const data = response['data']['value'];

  //         this.popularData = {
  //           labels: data.labels,
  //           series: data.series,
  //           icons: data.icons
  //         };

  //         this.loadPopularChart();
  //       }
  //     },
  //     error => {
  //       this._MsgHandelService.handleError(error);
  //     }
  //   );

  //   if (this.isAdmin) {
  //     // get sales
  //     this._InitService.getAllStores().subscribe(
  //       response => {
  //         if (this._MsgHandelService.handleSuccessResponse(response)) {
  //           this.saleStoreArray = response['data']['value'];
  //           this.saleStoreArray = this.saleStoreArray.map(sale => {
  //             return {
  //               id: sale._id,
  //               latitude: parseFloat(sale.latitude),
  //               longitude: parseFloat(sale.longitude),
  //               name: sale.name,
  //               address: sale.address,
  //               email: sale.email,
  //               phone: sale.phone,
  //               main_store: sale.main_store
  //             };
  //           });
  //         }
  //       },
  //       error => {
  //         this._MsgHandelService.handleError(error);
  //       }
  //     );

  //     // get profit
  //     this._InitService.getProfit('').subscribe(
  //       response => {
  //         if (this._MsgHandelService.handleSuccessResponse(response)) {
  //           const profit = response['data']['value'];
  //           this.createChart(profit);
  //         }
  //       },
  //       error => {
  //         this._MsgHandelService.handleError(error);
  //       }
  //     );

  //     // get rating
  //     this._InitService.allRating().subscribe(
  //       response => {
  //         if (this._MsgHandelService.handleSuccessResponse(response)) {
  //           const rating = response['data']['value']['avg_rating'];
  //           this.createMeter(rating);
  //         }
  //       },
  //       error => {
  //         this._MsgHandelService.handleError(error);
  //       }
  //     );

  //     // get monthly orders
  //     this._InitService.monthlyOrders('').subscribe(
  //       response => {
  //         if (this._MsgHandelService.handleSuccessResponse(response)) {
  //           const data = response['data']['value'];

  //           this.createBarChart(data);
  //         }
  //       },
  //       error => {
  //         this._MsgHandelService.handleError(error);
  //       }
  //     );
  //   } else {
  //     // get sales
  //     this._InitService
  //       .getSalesForAgent(this._JwtTokenValidatorService.getLoggedUserId())
  //       .subscribe(
  //         response => {
  //           if (this._MsgHandelService.handleSuccessResponse(response)) {
  //             this.saleStoreArray = response['data']['value'];
  //             this.saleStoreArray = this.saleStoreArray.map(sale => {
  //               return {
  //                 id: sale.id,
  //                 latitude: parseFloat(sale.latitude),
  //                 longitude: parseFloat(sale.longitude),
  //                 name: sale.name,
  //                 address: sale.address,
  //                 email: sale.email,
  //                 phone: sale.phone,
  //                 main_store: sale.main_store
  //               };
  //             });
  //           }
  //         },
  //         error => {
  //           this._MsgHandelService.handleError(error);
  //         }
  //       );

  //     // get profit
  //     this._InitService
  //       .getProfit(this._JwtTokenValidatorService.getLoggedUserId())
  //       .subscribe(
  //         response => {
  //           if (this._MsgHandelService.handleSuccessResponse(response)) {
  //             const profit = response['data']['value'];

  //             this.createChart(profit);
  //           }
  //         },
  //         error => {
  //           this._MsgHandelService.handleError(error);
  //         }
  //       );

  //     // get profit
  //     this._InitService
  //       .allAgentRating(this._JwtTokenValidatorService.getLoggedUserId())
  //       .subscribe(
  //         response => {
  //           if (this._MsgHandelService.handleSuccessResponse(response)) {
  //             const rating = response['data']['value']['avg_rating'];
  //             this.createMeter(rating);
  //           }
  //         },
  //         error => {
  //           this._MsgHandelService.handleError(error);
  //         }
  //       );

  //     // get monthly orders
  //     this._InitService
  //       .monthlyOrders(this._JwtTokenValidatorService.getLoggedUserId())
  //       .subscribe(
  //         response => {
  //           if (this._MsgHandelService.handleSuccessResponse(response)) {
  //             const data = response['data']['value'];

  //             this.createBarChart(data);
  //           }
  //         },
  //         error => {
  //           this._MsgHandelService.handleError(error);
  //         }
  //       );
  //   }
  // }

  private replaceZero(value: number) {
    if (value < 10) {
      return `0${value}`;
    } else {
      return value;
    }
  }

  // private createChart(profit) {
  //   profit['series'] = profit['series'].map(elem => elem / 1000000);

  //   this.chartOption2 = {
  //     xAxis: {
  //       type: 'category',
  //       data: profit['labels'],
  //       axisLabel: {
  //         color: '#ffffffcc'
  //       }
  //     },
  //     title: {
  //       subtext: 'PC - profit X 1 M'
  //     },
  //     tooltip: {
  //       trigger: 'axis'
  //     },
  //     yAxis: {
  //       type: 'value',
  //       name: 'PC',
  //       axisLine: {
  //         lineStyle: {
  //           color: '#fff'
  //         }
  //       }
  //     },
  //     color: ['#ffffff'],
  //     series: [
  //       {
  //         data: profit['series'],
  //         type: 'line',
  //         symbol: 'circle',
  //         symbolSize: 10,
  //         lineStyle: {
  //           color: 'white',
  //           width: 2,
  //           type: 'dashed'
  //         },
  //         itemStyle: {
  //           normal: {
  //             borderWidth: 3,
  //             borderColor: 'white',
  //             color: 'black'
  //           }
  //         }
  //       }
  //     ]
  //   };
  // }

  private loadPopularChart() {
    this.chartOption1 = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'none'
        },
        formatter: function (params) {
          return params[0].name + ': ' + params[0].value;
        }
      },
      xAxis: [
        {
          data: this.popularData.labels,
          axisTick: { show: false },
          axisLine: { show: false },
          axisLabel: {
            color: '#ffffffcc'
          }
        }
      ],
      yAxis: {
        splitLine: { show: false },
        axisTick: { show: false },
        axisLine: { show: false },
        axisLabel: { show: false }
      },
      color: ['#ffffff'],
      series: [
        {
          name: 'hill',
          type: 'pictorialBar',
          barCategoryGap: '-130%',
          // symbol: 'path://M0,10 L10,10 L5,0 L0,10 z',
          symbol: 'path://M0,10 L10,10 C5.5,10 5.5,5 5,0 C4.5,5 4.5,10 0,10 z',
          itemStyle: {
            normal: {
              opacity: 0.5
            },
            emphasis: {
              opacity: 1
            }
          },
          data: this.popularData.series,
          z: 10
        },
        {
          name: 'glyph',
          type: 'pictorialBar',
          barGap: '-100%',
          symbolPosition: 'end',
          symbolSize: 50,
          symbolOffset: [0, '-120%'],
          data: this.popularData.icons
        }
      ]
    };
  }

  // private createMeter(value) {
  //   this.chartOption4 = {
  //     tooltip: {
  //       formatter: '{a} <br/>{b} : {c}%'
  //     },

  //     series: [
  //       {
  //         name: 'Customer Rating',
  //         radius: '80%',
  //         type: 'gauge',
  //         detail: {
  //           formatter: '{value}%',
  //           fontSize: 20,
  //           textStyle: {
  //             color: '#fff'
  //           }
  //         },
  //         data: [{ value: value, name: '' }],

  //         axisLine: {
  //           lineStyle: {
  //             color: [[0.2, '#141f23'], [0.8, '#141f23'], [1, '#141f23']],
  //             width: 23
  //           }
  //         },
  //         axisLabel: {
  //           show: true,
  //           color: '#fff'
  //         },
  //         splitLine: {
  //           length: 20,
  //           lineStyle: {
  //             color: '#fff'
  //           }
  //         },
  //         pointer: {
  //           length: '80%',
  //           width: 8
  //         }
  //       }
  //     ]
  //   };
  // }

  // private createBarChart(result) {
  //   this.chartOption3 = {
  //     tooltip: {
  //       trigger: 'axis',
  //       axisPointer: {
  //         type: 'shadow'
  //       }
  //     },
  //     xAxis: {
  //       type: 'value',
  //       axisLine: {
  //         lineStyle: {
  //           color: '#fff'
  //         }
  //       }
  //     },
  //     yAxis: {
  //       type: 'category',
  //       axisLine: {
  //         lineStyle: {
  //           color: '#fff'
  //         }
  //       },

  //       axisTick: {
  //         lineStyle: {
  //           color: '#fff'
  //         }
  //       },
  //       data: result['labels'].reverse()
  //     },
  //     series: [
  //       {
  //         name: 'Orders',
  //         type: 'bar',
  //         itemStyle: {
  //           normal: {
  //             color: '#ffffffcc'
  //           },
  //           emphasis: {
  //             color: '#fff'
  //           }
  //         },
  //         data: result['series'].reverse()
  //       }
  //     ]
  //   };
  // }

  // private autoCompleteMap() {
  //   this._MapsAPILoader.load().then(() => {
  //     const autocomplete = new google.maps.places.Autocomplete(
  //       this.searchElementRef.nativeElement,
  //       {
  //         types: ['address']
  //       }
  //     );
  //     autocomplete.addListener('place_changed', () => {
  //       this._NgZone.run(() => {
  //         const place: google.maps.places.PlaceResult = autocomplete.getPlace();

  //         if (place.geometry === undefined || place.geometry === null) {
  //           return;
  //         }

  //         this.lat = place.geometry.location.lat();
  //         this.lng = place.geometry.location.lng();
  //         this.zoom = 10;
  //       });
  //     });
  //   });
  // }

  onMouseOver(infoWindow, $event: MouseEvent) {
    infoWindow.open();
  }

  private loadStyles() {
    this.styles = mapStyles;
    // this.styles = [];
  }
}
