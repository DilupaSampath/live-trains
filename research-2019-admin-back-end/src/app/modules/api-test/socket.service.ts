import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import * as io from 'socket.io-client';

// import env
import { environment } from '../../../environments/environment';
import { assembleI18nBoundString } from '@angular/compiler/src/render3/view/i18n';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private url = environment.socketUrl;
  socket: any;

  constructor() {
    this.socket = io(this.url);
  }
sendToSocket(){
    this.socket.emit('realtime-data', 
    {data:{
        
        trainId:"5d4b33776c91f72570b944c1",
         location: {
             start:{latitude:324324,longitude:234324},
             end:{latitude:324324,longitude:234324},
             current:{latitude:324324,longitude:234324}
         },
         time:10
    }
});
}
sendValidateSocket(data){
  this.socket.emit('validate-data', 
  {data:data
});
}
  
  getSocketAllMainValvesData() {
    const observable = new Observable(observer => {
      this.socket.on(
        'realTimeTable',
        data => {
          observer.next(data);
        },
        error => {
          console.log(error);
        }
      );
    });
    return observable;
  }
  getSocketValidateData() {
    const observable = new Observable(observer => {
      this.socket.on(
        'validateRes',
        data => {
          observer.next(data);
        },
        error => {
          console.log(error);
        }
      );
    });
    return observable;
  }
}