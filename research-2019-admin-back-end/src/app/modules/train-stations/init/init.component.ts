import { Component, OnInit } from '@angular/core';
import { InitService } from '../services/init/init.service';
import { MsgHandelService } from 'src/app/commons/services/msg-handel.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {
  searchText:any;
  updateId:any;
  isUpdate:boolean;
  trainStationForm: FormGroup;
  locations: any[] = [];
  trainStations: any[] = [];
  constructor( private _FormBuilder: FormBuilder, private _InitService: InitService, private _MsgHandelService: MsgHandelService) { 
    this.trainStationForm = this._FormBuilder.group({
      location: [null, Validators.compose([Validators.required])],
      locationName: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.getLocations();
    this.getAllStations();
  }
  changeState(event:any,status:boolean){
    console.log(event);
this.isUpdate=status;
this.updateId=event;
  }
getLocations(){

  this._InitService.getAlLocations().subscribe(
    Response => {
      if (this._MsgHandelService.handleSuccessResponse(Response)) {
        this.locations = Response['data'];
        console.log(" this.allLocations");
        console.log( this.locations);
      }
    },
    error => {
      this._MsgHandelService.handleError(error);
    }
  );
}
getAllStations(){

  this._InitService.getAll().subscribe(
    Response => {
      console.log("Response this.trainStation");
      console.log(Response);

      if (this._MsgHandelService.handleSuccessResponse(Response)) {
        this.trainStations = Response['data'];
        console.log(" this.trainStation");
        console.log( this.trainStations);
      }
    },
    error => {
      this._MsgHandelService.handleError(error);
    }
  );
}

createStation(){
 console.log(this.trainStationForm.controls['location'].value);
  swal({
    title: 'Are you sure?',
    text: 'Do you need save this train station!',
    type: 'info',
    showCancelButton: true,
    showLoaderOnConfirm: true
  }).then(result => {
    if (result.value) {
      let obj = {
        "name":this.trainStationForm.controls['locationName'].value,
"location":this.trainStationForm.controls['location'].value
      };
      this._InitService.postObj(obj).subscribe(
        locationResponse => {
          if (this._MsgHandelService.handleSuccessResponse(locationResponse)) {
            this._MsgHandelService.showSuccessMsg(
              '',
              'Successfully saved the location!'
            );
            // this._Router.navigateByUrl('/locations');
            this.getLocations();
            this.getAllStations();
          }
        },
        error => {
          this._MsgHandelService.handleError(error);
        }
      );
    }
  });
  
}

updateStation(){
  console.log(this.trainStationForm.controls['location'].value);
   swal({
     title: 'Are you sure?',
     text: 'Do you need update this train station!',
     type: 'info',
     showCancelButton: true,
     showLoaderOnConfirm: true
   }).then(result => {
     if (result.value) {
       let obj = {
         "id":this.updateId,
         "name":this.trainStationForm.controls['locationName'].value,
 "location":this.trainStationForm.controls['location'].value
       };
       this._InitService.putObj(obj).subscribe(
         locationResponse => {
           if (this._MsgHandelService.handleSuccessResponse(locationResponse)) {
             this._MsgHandelService.showSuccessMsg(
               '',
               'Successfully updated the location!'
             );
             // this._Router.navigateByUrl('/locations');
             this.getLocations();
             this.getAllStations();
           }
         },
         error => {
           this._MsgHandelService.handleError(error);
         }
       );
     }
   });
   
 }

 deleteStation(trainStation:any){
  let obj={"id":trainStation._id};
  swal({
    title: 'Are you sure?',
    text: 'Do you need to delete this train station!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    confirmButtonText: 'Yes',
    buttonsStyling: false
  }).then(result => {
    if (result.value) {
      this._InitService.deleteObj(obj).subscribe(
        data => {
          if (this._MsgHandelService.handleSuccessResponse(data)) {
            this._MsgHandelService.showSuccessMsg(
              '',
              'Train station successfully deleted !'
            );
            this.getLocations();
             this.getAllStations();
          }
        },
        err => {
          this._MsgHandelService.handleError(err);
        }
      );
    }
  });

}
}
