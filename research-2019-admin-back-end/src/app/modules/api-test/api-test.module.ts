import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { AppMaterialModule } from "../../app.material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { _Routes } from "./api-test.routing";

/* services */
import { InitService } from "./services/init/init.service";

/* components */
import { InitComponent } from "./init/init.component";
import { GoogleMapsAPIWrapper, AgmCoreModule } from "@agm/core";
import { AgmDirectionModule } from 'agm-direction';
import { SocketService } from "./socket.service";
import { BestScoreManager } from "./game.storage.service";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppMaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild(_Routes),
    AgmCoreModule.forRoot({ // @agm/core
      apiKey: 'AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8',
    }),
    AgmDirectionModule      // agm-direction

  ],
  declarations: [InitComponent],
  providers: [InitService,GoogleMapsAPIWrapper,SocketService,BestScoreManager]
})
export class ApiTestModule {}