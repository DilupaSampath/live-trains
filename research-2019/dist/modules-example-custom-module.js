(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-example-custom-module"],{

/***/ "./src/app/modules/example/custom.module.ts":
/*!**************************************************!*\
  !*** ./src/app/modules/example/custom.module.ts ***!
  \**************************************************/
/*! exports provided: CustomModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomModule", function() { return CustomModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _app_material_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../app.material.module */ "./src/app/app.material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _custom_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./custom.routing */ "./src/app/modules/example/custom.routing.ts");
/* harmony import */ var _services_init_init_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/init/init.service */ "./src/app/modules/example/services/init/init.service.ts");
/* harmony import */ var _init_init_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./init/init.component */ "./src/app/modules/example/init/init.component.ts");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./filter.pipe */ "./src/app/modules/example/filter.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






/* services */

/* components */


var CustomModule = /** @class */ (function () {
    function CustomModule() {
    }
    CustomModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _app_material_module__WEBPACK_IMPORTED_MODULE_3__["AppMaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_custom_routing__WEBPACK_IMPORTED_MODULE_5__["_Routes"])
            ],
            declarations: [_init_init_component__WEBPACK_IMPORTED_MODULE_7__["InitComponent"], _filter_pipe__WEBPACK_IMPORTED_MODULE_8__["FilterPipe"]],
            providers: [_services_init_init_service__WEBPACK_IMPORTED_MODULE_6__["InitService"]]
        })
    ], CustomModule);
    return CustomModule;
}());



/***/ }),

/***/ "./src/app/modules/example/custom.routing.ts":
/*!***************************************************!*\
  !*** ./src/app/modules/example/custom.routing.ts ***!
  \***************************************************/
/*! exports provided: _Routes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_Routes", function() { return _Routes; });
/* harmony import */ var _init_init_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./init/init.component */ "./src/app/modules/example/init/init.component.ts");
// component import

var _Routes = [
    {
        path: "",
        component: _init_init_component__WEBPACK_IMPORTED_MODULE_0__["InitComponent"]
    }
];


/***/ }),

/***/ "./src/app/modules/example/filter.pipe.ts":
/*!************************************************!*\
  !*** ./src/app/modules/example/filter.pipe.ts ***!
  \************************************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toLowerCase();
        return items.filter(function (it) {
            return it.name.toLowerCase().includes(searchText);
        });
    };
    FilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter'
        })
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/modules/example/init/init.component.css":
/*!*********************************************************!*\
  !*** ./src/app/modules/example/init/init.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modules/example/init/init.component.html":
/*!**********************************************************!*\
  !*** ./src/app/modules/example/init/init.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"col-md-12\">\r\n    <div class=\"card \">\r\n      <div class=\"card-header card-header-info card-header-icon\">\r\n        <div class=\"card-icon\">\r\n          <i class=\"material-icons\">build</i>\r\n          <h4 class=\"card-title\">Locations</h4>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body \">\r\n\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n            <div class=\"card\">\r\n              <div class=\"card-header\">\r\n                <form>\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-6\"  *ngIf=\"!status\">\r\n                      <mat-form-field class=\"example-full-width\">\r\n                          <input  matInput \r\n              type=\"text\"\r\n              [(ngModel)]=\"autocompleteInput\" placeholder=\"{{searchStatus}}\"\r\n              #addresstext \r\n              (setAddress)=\"getAddress($event)\"\r\n              [ngModelOptions]=\"{standalone: true}\"\r\n              >\r\n                      </mat-form-field>\r\n                    </div>\r\n                    <div class=\"col-md-6\"  *ngIf=\"status\">\r\n                      <mat-form-field class=\"example-full-width\">\r\n                          <input  matInput \r\n              type=\"text\"\r\n              [(ngModel)]=\"searchText\" placeholder=\"{{searchStatus}}\"\r\n              [ngModelOptions]=\"{standalone: true}\"\r\n              #addresstext \r\n              (setAddress)=\"getAddress($event)\"\r\n              >\r\n                      </mat-form-field>\r\n                    </div>\r\n<div class=\"col-md-6\" *ngIf=\"!status\">\r\n    <button mat-mini-fab aria-label=\"Example icon-button with a heart icon\">\r\n        <mat-icon (click)=\"createLocation()\">add</mat-icon>\r\n      </button>\r\n     \r\n</div>\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <mat-slide-toggle (change)=\"toggle($event)\">Change action to search or create location</mat-slide-toggle>\r\n  </div>\r\n</div>\r\n                  </div>\r\n                </form>\r\n              </div>\r\n              <div class=\"card-body\">\r\n                <div class=\"table-responsive\">\r\n                  <table class=\"table table-hover\">\r\n                    <thead class=\"\">\r\n                   \r\n                      <th>\r\n                        Location Name\r\n                      </th>\r\n                      <th>\r\n                        Latitude\r\n                      </th>\r\n                      <th>\r\n                        Longitude\r\n                      </th>\r\n                     \r\n                      <th>\r\n                        Action\r\n                      </th>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr *ngFor=\"let location of allLocations | filter : searchText\" >\r\n               \r\n                        <td>\r\n                          {{location.name}}\r\n                        </td>\r\n                        <td>\r\n                          {{location.location.coordinates[1]}} \r\n                        </td>\r\n                        <td>\r\n                          {{location.location.coordinates[0]}} \r\n                        </td>\r\n                  \r\n                        <td>\r\n                          <div class=\"row justify-content-start\">\r\n\r\n                            <i class=\"material-icons\" (click)=\"deleteLocation(location)\">\r\n                              delete\r\n                            </i>\r\n\r\n                            &nbsp;\r\n                            <!-- matTooltip=\"View\"\r\n                            data-toggle=\"modal\"\r\n                            data-target=\"#myModal1\" -->\r\n                              <i class=\"material-icons\"\r\n                              matTooltip=\"View\"\r\n                            data-toggle=\"modal\"\r\n                            data-target=\"#myModal1\">\r\n                                edit\r\n                              </i>\r\n                            \r\n                            &nbsp;\r\n                            <i class=\"material-icons\">\r\n                              desktop_windows\r\n                            </i>\r\n\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n</div>\r\n\r\n\r\n<div\r\n  class=\"modal fade\"\r\n  id=\"myModal1\"\r\n  tabindex=\"-1\"\r\n  role=\"dialog\"\r\n  aria-labelledby=\"myModalLabel\"\r\n  aria-hidden=\"true\"\r\n>\r\n  <div class=\"modal-dialog\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"card-body modal-body\">\r\n          <div class=\"example-container\">\r\n              \r\n            </div>\r\n         \r\n              <button\r\n                mat-raised-button\r\n                type=\"submit\"\r\n                class=\"btn btn-primary pull-right\"\r\n              >\r\n                Save\r\n              </button>\r\n          </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- end of view toggle window -->\r\n<!-- <button\r\n                  class=\"pause-btn\"\r\n                  mat-icon-button\r\n                  matTooltip=\"View\"\r\n                  style=\"    color: rgb(26, 38, 42) !important;\"\r\n                  data-toggle=\"modal\"\r\n                  data-target=\"#myModal1\"\r\n                >\r\n                  <mat-icon aria-label=\"Example icon-button text-warning\"\r\n                    >visibility</mat-icon\r\n                  >\r\n                </button> -->"

/***/ }),

/***/ "./src/app/modules/example/init/init.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/example/init/init.component.ts ***!
  \********************************************************/
/*! exports provided: InitComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitComponent", function() { return InitComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_init_init_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/init/init.service */ "./src/app/modules/example/services/init/init.service.ts");
/* harmony import */ var src_app_commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/commons/services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InitComponent = /** @class */ (function () {
    function InitComponent(_Router, _InitService, _MsgHandelService) {
        this._Router = _Router;
        this._InitService = _InitService;
        this._MsgHandelService = _MsgHandelService;
        this.searchStatus = "Create Location";
        this.status = false;
        this.setAddress = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    InitComponent.prototype.ngAfterViewInit = function () {
        this.getPlaceAutocomplete();
    };
    InitComponent.prototype.getPlaceAutocomplete = function () {
        var _this = this;
        var autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement, {
            componentRestrictions: { country: 'LK' },
            types: [this.adressType] // 'establishment' / 'address' / 'geocode'
        });
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            _this.invokeEvent(place);
        });
    };
    InitComponent.prototype.toggle = function (event) {
        console.log('toggle', event.checked);
        if (event.checked) {
            this.status = true;
            this.searchStatus = "Search Location";
        }
        else {
            this.status = false;
            this.searchStatus = "Create Location";
        }
    };
    InitComponent.prototype.getAddress = function (place) {
        console.log(place['formatted_address']);
    };
    InitComponent.prototype.invokeEvent = function (place) {
        this.location = {
            "name": place.adr_address,
            "location_type": "train",
            "location": {
                "type": "Point",
                "coordinates": [place.geometry.location.lat(), place.geometry.location.lng()]
            }
        };
        console.log(this.location);
        this.setAddress.emit(place);
    };
    InitComponent.prototype.ngOnInit = function () {
        this.getAllLocationData();
    };
    InitComponent.prototype.getAllLocationData = function () {
        var _this = this;
        this._InitService.getAll().subscribe(function (Response) {
            if (_this._MsgHandelService.handleSuccessResponse(Response)) {
                _this.allLocations = Response['data'];
                console.log(" this.allLocations");
                console.log(_this.allLocations);
            }
        }, function (error) {
            _this._MsgHandelService.handleError(error);
        });
    };
    InitComponent.prototype.createLocation = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()({
            title: 'Are you sure?',
            text: 'Do you need save this location!',
            type: 'info',
            showCancelButton: true,
            showLoaderOnConfirm: true
        }).then(function (result) {
            if (result.value) {
                _this._InitService.postObj(_this.location).subscribe(function (locationResponse) {
                    if (_this._MsgHandelService.handleSuccessResponse(locationResponse)) {
                        _this._MsgHandelService.showSuccessMsg('', 'Successfully saved the location!');
                        // this._Router.navigateByUrl('/locations');
                        _this.getAllLocationData();
                    }
                }, function (error) {
                    _this._MsgHandelService.handleError(error);
                });
            }
        });
    };
    InitComponent.prototype.deleteLocation = function (location) {
        var _this = this;
        var obj = { "id": location._id };
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()({
            title: 'Are you sure?',
            text: 'Do you need to delete this location!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                _this._InitService.deleteObj(obj).subscribe(function (data) {
                    if (_this._MsgHandelService.handleSuccessResponse(data)) {
                        _this._MsgHandelService.showSuccessMsg('', 'Location successfully deleted !');
                        _this.getAllLocationData();
                    }
                }, function (err) {
                    _this._MsgHandelService.handleError(err);
                });
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], InitComponent.prototype, "adressType", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], InitComponent.prototype, "setAddress", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('addresstext'),
        __metadata("design:type", Object)
    ], InitComponent.prototype, "addresstext", void 0);
    InitComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-init',
            template: __webpack_require__(/*! ./init.component.html */ "./src/app/modules/example/init/init.component.html"),
            styles: [__webpack_require__(/*! ./init.component.css */ "./src/app/modules/example/init/init.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_init_init_service__WEBPACK_IMPORTED_MODULE_1__["InitService"], src_app_commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_2__["MsgHandelService"]])
    ], InitComponent);
    return InitComponent;
}());



/***/ }),

/***/ "./src/app/modules/example/services/init/init.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/modules/example/services/init/init.service.ts ***!
  \***************************************************************/
/*! exports provided: InitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitService", function() { return InitService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _infrastructure_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../infrastructure/api.service */ "./src/app/infrastructure/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import common service

var InitService = /** @class */ (function () {
    function InitService(_apiService) {
        this._apiService = _apiService;
        this.loggedUser = window.localStorage.getItem('userId');
    }
    InitService.prototype.getAll = function () {
        return this._apiService.get("location/getAll");
    };
    InitService.prototype.getOne = function () {
        return this._apiService.get("api/feedback/getOne/" + this.loggedUser);
    };
    InitService.prototype.postObj = function (obj) {
        return this._apiService.post("location/create", obj);
    };
    InitService.prototype.putObj = function (obj) {
        return this._apiService.post("location/update", obj);
    };
    InitService.prototype.deleteObj = function (obj) {
        return this._apiService.post("location/delete", obj);
    };
    InitService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_infrastructure_api_service__WEBPACK_IMPORTED_MODULE_1__["MainService"]])
    ], InitService);
    return InitService;
}());



/***/ })

}]);
//# sourceMappingURL=modules-example-custom-module.js.map