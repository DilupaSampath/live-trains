const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const locationSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true
        },
        location_type:{ type: String },
        location: {
            type: { type: String },
            coordinates: []
           },

    }, { timestamps: true }
);
locationSchema.index({ location: "2dsphere" });
module.exports = mongoose.model('locations', locationSchema);