import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.localStorage.getItem('userId');

  getAll(): Observable<any> {
    return this._apiService.get(`location/getAll`);
  }

  getOne(): Observable<any> {
    return this._apiService.get(`api/feedback/getOne/${this.loggedUser}`);
  }

  postObj(obj): Observable<any> {
    return this._apiService.post(`location/create`, obj);
  }

  putObj(obj): Observable<any> {
    return this._apiService.post(`location/update`, obj);
  }

  deleteObj(obj): Observable<any> {
    return this._apiService.post(`location/delete`, obj);
  }
}
