import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InitService } from '../services/init/init.service';
import { MsgHandelService } from 'src/app/commons/services/msg-handel.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {
  searchText:any;
  updateId:any;
  isUpdate:boolean;
  timeTableForm: FormGroup;
  locations: any[] = [];
  trains: any[] = [];
  timeTables: any[] = [];

  trainStations: any[] = [];
  constructor( private _FormBuilder: FormBuilder, private _InitService: InitService, private _MsgHandelService: MsgHandelService) { 
    this.timeTableForm = this._FormBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      train: [null, Validators.compose([Validators.required])],
      start: [null, Validators.compose([Validators.required])],
      end: [null, Validators.compose([Validators.required])],
      distance: [null, null],
      start_time: [null, Validators.compose([Validators.required])],
      end_time: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.getAlltrainData();
    this.getAllLocationData();
    this.getAllTimeTableData();
  }
  changeState(event:any,status:boolean){
    console.log(event);
this.isUpdate=status;
this.updateId=event;
  }
  getAllLocationData(){
    this._InitService.getAllLocations().subscribe(
      Response => {
        if (this._MsgHandelService.handleSuccessResponse(Response)) {
          this.locations = Response['data'];
          console.log(" this.allLocations");
          console.log( this.locations);
        }
      },
      error => {
        this._MsgHandelService.handleError(error);
      }
    );
  }
  getAllTimeTableData(){
    this._InitService.getAll().subscribe(
      Response => {
        if (this._MsgHandelService.handleSuccessResponse(Response)) {
          this.timeTables = Response['data'];
          console.log(" this.timeTables");
          console.log( this.timeTables);
        }
      },
      error => {
        this._MsgHandelService.handleError(error);
      }
    );
  }
  getAlltrainData(){
    this._InitService.getAllTrains().subscribe(
      Response => {
        if (this._MsgHandelService.handleSuccessResponse(Response)) {
          this.trains = Response['data'];
          console.log(" this.allLocations");
          console.log( this.trains);
        }
      },
      error => {
        this._MsgHandelService.handleError(error);
      }
    );
  }

  createTimeTable(){
    console.log(this.timeTableForm);
    console.log(new Date(this.timeTableForm.controls['start_time'].value));
    let startTime:Date=new Date(this.timeTableForm.controls['start_time'].value);
    let endTime:Date=new Date(this.timeTableForm.controls['end_time'].value);
     swal({
       title: 'Are you sure?',
       text: 'Do you need save time slot!',
       type: 'info',
       showCancelButton: true,
       showLoaderOnConfirm: true
     }).then(result => {
       if (result.value) {
        let obj = {
          "id":this.updateId,
          "name":this.timeTableForm.controls['name'].value,
          "train":this.timeTableForm.controls['train'].value,
  "start":this.timeTableForm.controls['start'].value,
  "end":this.timeTableForm.controls['end'].value,
  "start_time": new Date(this.timeTableForm.controls['start_time'].value).getTime(),
  "end_time":new Date(this.timeTableForm.controls['end_time'].value).getTime(),
  "short_start_time": startTime.getHours()+":"+startTime.getMinutes()+":"+startTime.getSeconds(),
  "short_end_time":endTime.getHours()+":"+endTime.getMinutes()+":"+endTime.getSeconds(),
  // "start":this.timeTableForm.controls['stations'].value,
        };
    console.log(obj);

         this._InitService.postObj(obj).subscribe(
           locationResponse => {
             if (this._MsgHandelService.handleSuccessResponse(locationResponse)) {
               this._MsgHandelService.showSuccessMsg(
                 '',
                 'Successfully saved the time slot!'
               );
               this.getAlltrainData();
               this.getAllLocationData();
               this.getAllTimeTableData();
             }
           },
           error => {
             this._MsgHandelService.handleError(error);
           }
         );
       }
     });
     
   }
   
   updateTimeTable(){
     console.log(new Date(this.timeTableForm.controls['start_time'].value));
     let startTime:Date=new Date(this.timeTableForm.controls['start_time'].value);
     let endTime:Date=new Date(this.timeTableForm.controls['end_time'].value);
      swal({
        title: 'Are you sure?',
        text: 'Do you need update this time table!',
        type: 'info',
        showCancelButton: true,
        showLoaderOnConfirm: true
      }).then(result => {
        if (result.value) {
          let obj = {
            "id":this.updateId,
            "name":this.timeTableForm.controls['name'].value,
    "start":this.timeTableForm.controls['start'].value,
    "end":this.timeTableForm.controls['end'].value,
    "start_time": new Date(this.timeTableForm.controls['start_time'].value).getTime(),
    "end_time":new Date(this.timeTableForm.controls['end_time'].value).getTime(),
    "short_start_time": startTime.getHours()+":"+startTime.getMinutes()+":"+startTime.getSeconds(),
    "short_end_time":endTime.getHours()+":"+endTime.getMinutes()+":"+endTime.getSeconds(),
    // "start":this.timeTableForm.controls['stations'].value,
          };
          this._InitService.putObj(obj).subscribe(
            locationResponse => {
              if (this._MsgHandelService.handleSuccessResponse(locationResponse)) {
                this._MsgHandelService.showSuccessMsg(
                  '',
                  'Successfully updated the time table!'
                );
                // this._Router.navigateByUrl('/locations');
                this.getAlltrainData();
                this.getAllLocationData();
                this.getAllTimeTableData();
              }
            },
            error => {
              this._MsgHandelService.handleError(error);
            }
          );
        }
      });
      
    }

    deleteTimeTable(timeTable:any){
      let obj={"id":timeTable._id};
      swal({
        title: 'Are you sure?',
        text: 'Do you need to delete this time slot!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Yes',
        buttonsStyling: false
      }).then(result => {
        if (result.value) {
          this._InitService.deleteObj(obj).subscribe(
            data => {
              if (this._MsgHandelService.handleSuccessResponse(data)) {
                this._MsgHandelService.showSuccessMsg(
                  '',
                  'Time slot successfully deleted !'
                );
                this.getAlltrainData();
                this.getAllLocationData();
                this.getAllTimeTableData();
              }
            },
            err => {
              this._MsgHandelService.handleError(err);
            }
          );
        }
      });
    
    }
}
