import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

/**
 *  import service
 */
import { AuthService } from '../../services/authentication.service';
import { JwtService } from '../../../infrastructure/jwt.service';
import { LocalStorageHandleService } from '../../services/localStorageHandle.service';

// import form modules
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * import common services
 */
import { MsgHandelService } from '../../../commons/services/msg-handel.service';
import { JwtTokenValidatorService } from '../../services/jwt-token-validator.service';

// Jquery
declare let $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  // form values
  rForm: FormGroup;

  test: Date = new Date();
  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;
  user: any = {};
  loading_gif: any;
  loading_info: any;

  constructor(
    private _MsgHandelService: MsgHandelService,
    private _ElementRef: ElementRef,
    private _FormBuilder: FormBuilder,
    private _Router: Router,
    private _AuthService: AuthService,
    private _JwtService: JwtService,
    private _JwtTokenValidatorService: JwtTokenValidatorService,
    private _LocalStorageHandleService: LocalStorageHandleService
  ) {
    this.nativeElement = _ElementRef.nativeElement;
    this.sidebarVisible = false;
    // form validation
    this.rForm = this._FormBuilder.group({
      username: [
        null,
        Validators.compose([Validators.required, Validators.email])
      ],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    /**
     * redirect to dashboard if the user is already logged in
     */
    if (this._JwtService.getToken()) {
      this._Router.navigateByUrl('/dashboard');
    }

    this.loading_gif = document.getElementById('loading-div');
    this.loading_info = document.getElementById('loading-info');
    const navbar: HTMLElement = this._ElementRef.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    body.classList.add('off-canvas-sidebar');
    const card = document.getElementsByClassName('card')[0];
    setTimeout(function() {
      // after 1000 ms we add the class animated to the login/register card
      card.classList.remove('card-hidden');
    }, 700);
  }
  sidebarToggle() {
    const toggleButton = this.toggleButton;
    const body = document.getElementsByTagName('body')[0];
    const sidebar = document.getElementsByClassName('navbar-collapse')[0];
    if (this.sidebarVisible === false) {
      setTimeout(function() {
        toggleButton.classList.add('toggled');
      }, 500);
      body.classList.add('nav-open');
      this.sidebarVisible = true;
    } else {
      this.toggleButton.classList.remove('toggled');
      this.sidebarVisible = false;
      body.classList.remove('nav-open');
    }
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    body.classList.remove('off-canvas-sidebar');
  }

  forgetPassword() {
    swal({
      title: 'Are you sure?',
      text: 'Your current password will be reset from this action',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes',
      buttonsStyling: false
    }).then(willDelete => {
      if (willDelete.value) {
        this._AuthService
          .forgerPassword({ user_name: this.rForm.controls['username'].value })
          .subscribe(
            response => {
              if (this._MsgHandelService.handleSuccessResponse(response)) {
                this._MsgHandelService.showSuccessMsg(
                  '',
                  'Please check your email!'
                );
              }
            },
            error => {
              this._MsgHandelService.handleError(error);
            }
          );
      }
    });
  }

  // login function
  userLogin(post) {
    // create login array
    const userObj = {
      email: post.username,
      password: post.password
    };

    this._AuthService.loginUser(userObj).subscribe(
      response => {
        if (this._MsgHandelService.handleSuccessResponse(response)) {
          console.log(response);
          // store token
          this._JwtService.saveToken(response.token);

          // if (
          //   this._JwtTokenValidatorService.validateUserRole('admin') ||
          //   this._JwtTokenValidatorService.validateUserRole('agent')
          // ) {
          //   // show msg
          //   this._MsgHandelService.showSuccessMsg(
          //     'Welcome to Live Trains!',
          //     'Successfully logged in!'
          //   );

            // load the dashboard
            this._Router.navigateByUrl('/dashboard');
          // } else {
          //   this._MsgHandelService.showErrorMsg(
          //     '',
          //     'You are not authorized to login'
          //   );
          //   localStorage.removeItem('token');
          // }
        }
      },
      error => {
        this._MsgHandelService.handleError(error);
      }
    );
  }
}
