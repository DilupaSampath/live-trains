const timeTableModel = require('./timeTableModel');

/**
 * find from the system
 */
module.exports.findTimeTable = (condition) => {
    return new Promise((resolve, reject) => {
        timeTableModel.findOne(condition).populate('end').populate('start').populate('train').exec((err, location) => {
            err ? reject(err) : resolve(location);
        });
    })
};

module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        timeTableModel.find(condition).populate('train').populate('start').populate('end').exec((err, location) => {
            err ? reject(err) : resolve(location);
        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        timeTableModel.findOneAndDelete({ _id: id },
            async (err, location) => {
                if (err)
                    reject(err);
                else if (location != null || location != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let item = new timeTableModel();
            item.name = data.name;
            item.current_location = data.current_location;
            item.train = data.train;
            item.start = data.start;
            item.end = data.end;
            item.distance = data.distance;
            item.isRunning = data.isRunning;
            item.start_time = data.start_time;
            item.end_time = data.end_time;
            item.short_start_time = data.short_start_time;
            item.short_end_time = data.short_end_time;

            
            item.save(async (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(data);
                }
            })
        } catch (error) {
            reject('' + error)
        }
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        timeTableModel.findOne({ _id: id }).populate('train')
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = (id, data) => {
    return new Promise((resolve, reject) => {
        timeTableModel.findByIdAndUpdate(id, data, { new: true })
            .exec((err, result) => {
                if (err){
                    console.log(JSON.stringify(err));
                    reject(err)
                }
                else if (result == null || result == undefined)
                    {
                        // console.log(JSON.stringify(err));
                        reject("Invalid id");
                    }
                else
                    {
                        console.log("Updated *************");
                    resolve(result);
                }
            });
    })
};
