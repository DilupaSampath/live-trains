'use strict';
// Import Express
const express = require('express');
// user router
const router = express.Router();
// Import body parser
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json({ limit: '50mb' }));
router.use(bodyParser.json());

// import  controllers
const userController = require('../src/user/userController');
const locationController = require('../src/location/locationController');
const trainController = require('../src/train/trainController');
const trainStationController = require('../src/train-station/trainStationController');
const timeTableController = require('../src/time-table/timeTableController');
const googleApiController = require('../src/google-api/googleApiController');
const logController = require('../src/logs/logController');


// import validator Schemas
const userSchema = require('../src/user/userSchema');
const locationSchema = require('../src/location/locationSchema');
const trainSchema = require('../src/train/trainSchema');
const timeTableSchema = require('../src/time-table/timeTableSchema');
const googleApiSchema = require('../src/google-api/googleApiSchema');
const logSchema = require('../src/logs/logSchema');
const trainStationSchema = require('../src/train-station/trainStationSchema');

// import Validator class
const validator = require('../services/validator');

//user routes
router.route('/user/new')
    .post(validator.validateBody(userSchema.newUser), userController.newUser);
router.route('/user/login')
    .post(validator.validateBody(userSchema.login), userController.login);

//location routes
router.route('/location/getAll')
    .get(locationController.getAll);
router.route('/location/findOne')
    .get(locationController.findOne);
router.route('/location/create')
    .post(validator.validateBodyWithToken(locationSchema.create),
        locationController.create);
router.route('/location/update')
    .post(validator.validateBodyWithToken(locationSchema.update),
        locationController.update);
router.route('/location/delete')
    .post(validator.validateBodyWithToken(locationSchema.id),
        locationController.remove);

//train routes
router.route('/train/getAll')
    .get(trainController.getAll);
router.route('/train/findOne')
    .post(validator.validateBody(trainSchema.id),trainController.findOne);
router.route('/train/create')
    .post(validator.validateBody(trainSchema.create),
        trainController.create);
router.route('/train/update')
    .post(validator.validateBodyWithToken(trainController.update),
        trainController.update);
router.route('/train/delete')
    .post(validator.validateBody(trainSchema.id), trainController.remove);


//train station routes
router.route('/trainStation')
    .get(trainStationController.getAll);
router.route('/trainStation/findOne')
    .get(trainStationController.findOne);
router.route('/trainStation/create')
    .post(validator.validateBody(trainStationSchema.create),
    trainStationController.create);
router.route('/trainStation/update')
    .post(validator.validateBodyWithToken(trainStationSchema.update),
    trainStationController.update);
router.route('/trainStation/delete')
    .post(validator.validateBodyWithToken(trainStationSchema.id),
    trainStationController.remove);

//time table routes
router.route('/timeTable')
    .get(timeTableController.getAll);
router.route('/timeTable/findOneByCondition')
    .post(timeTableController.findOneByCondition);
router.route('/timeTable/findOne')
    .post(validator.validateBody(timeTableSchema.id),timeTableController.findOne);
router.route('/timeTable/create')
    .post(validator.validateBody(timeTableSchema.create),
    timeTableController.create);
router.route('/timeTable/update')
    .post(validator.validateBodyWithToken(timeTableSchema.update),
    timeTableController.update);
router.route('/timeTable/delete')
    .post(validator.validateBodyWithToken(timeTableSchema.id),
    timeTableController.remove);
//Log routes
router.route('/logs/operation')
    .post(validator.validateBody(logSchema.limit),
        logController.getOperationLogs);
router.route('/logs/mainValve')
    .post(validator.validateBody(logSchema.limit),
        logController.getMainValveLogs);
router.route('/logs/rackValve')
    .post(validator.validateBody(logSchema.limit),
        logController.getrackLogs);
router.route('/logs/pump')
    .post(validator.validateBody(logSchema.limit),
        logController.getPumpLogs);
router.route('/logs/co2')
    .post(validator.validateBody(logSchema.limit),
        logController.getCo2Logs);

//google api routes
router.route('/googleApi/getSpeed')
    .post(googleApiController.getSpeed);
router.route('/googleApi/getDistance')
    .post(googleApiController.getDistance);

router.route('/googleApi/getNearByPlaces')
    .post(googleApiController.getNearByPlaces);
    
router.route('/googleApi/getDataWithArrivelTime')
.post(googleApiController.getDataWithArrivelTime);

router.route('/googleApi/validatePoint').post(googleApiController.validatePointAndPath);

module.exports = router;