import { Routes } from "@angular/router";

// component import
import { InitComponent } from "./init/init.component";

export const _Routes: Routes = [
  {
    path: "",
    component: InitComponent
  }
];
