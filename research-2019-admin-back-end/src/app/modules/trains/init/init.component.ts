import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { InitService } from '../services/init/init.service';
import { MsgHandelService } from 'src/app/commons/services/msg-handel.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {
  searchText:any;
  selectedFile:any;
  updateId:any;
  isUpdate:boolean;
  trains: FormGroup;
  toppings = new FormControl();
  stations: any[];
  alTrains: any[];
  constructor(private _FormBuilder: FormBuilder, private _InitService: InitService ,private _MsgHandelService: MsgHandelService) {
    this.trains = this._FormBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      stations: [null, Validators.compose([Validators.required])]
    });
   }

  ngOnInit() {
    this.getTrains();
    this.getAllStations();
  }
  changeState(event:any,status:boolean){
    console.log(event);
this.isUpdate=status;
this.updateId=event;
  }
  getTrains(){

    this._InitService.getAllTrains().subscribe(
      Response => {
        if (this._MsgHandelService.handleSuccessResponse(Response)) {
          this.alTrains = Response['data'];
          console.log(" this.alTrains");
          console.log( this.alTrains);
        }
      },
      error => {
        this._MsgHandelService.handleError(error);
      }
    );
  }
  getAllStations(){

    this._InitService.getAll().subscribe(
      Response => {
        console.log("Response this.trainStation");
        console.log(Response);
  
        if (this._MsgHandelService.handleSuccessResponse(Response)) {
          this.stations = Response['data'];
          console.log(" this.trainStation");
          console.log( this.stations);
        }
      },
      error => {
        this._MsgHandelService.handleError(error);
      }
    );
  }
  onSelectFile(event) {
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
    const fileReader:any = new FileReader();
    fileReader.readAsText(this.selectedFile, "UTF-8");
    fileReader.onload = () => {
     console.log(JSON.parse(fileReader.result));
    }
    fileReader.onerror = (error) => {
      console.log(error);
    }
  }
  createTrain(){
    console.log(this.trains.controls['stations'].value);
     swal({
       title: 'Are you sure?',
       text: 'Do you need save this train!',
       type: 'info',
       showCancelButton: true,
       showLoaderOnConfirm: true
     }).then(result => {
       if (result.value) {
         let obj = {
           "name":this.trains.controls['name'].value,
   "train_stations":this.trains.controls['stations'].value
         };
         this._InitService.postObj(obj).subscribe(
           locationResponse => {
             if (this._MsgHandelService.handleSuccessResponse(locationResponse)) {
               this._MsgHandelService.showSuccessMsg(
                 '',
                 'Successfully saved the location!'
               );
               // this._Router.navigateByUrl('/locations');
               this.getTrains();
               this.getAllStations();
             }
           },
           error => {
             this._MsgHandelService.handleError(error);
           }
         );
       }
     });
     
   }
   
   updateTrain(){
     console.log(this.trains.controls['stations'].value);
      swal({
        title: 'Are you sure?',
        text: 'Do you need update this train!',
        type: 'info',
        showCancelButton: true,
        showLoaderOnConfirm: true
      }).then(result => {
        if (result.value) {
          let obj = {
            "id":this.updateId,
            "name":this.trains.controls['name'].value,
    "location":this.trains.controls['stations'].value
          };
          this._InitService.putObj(obj).subscribe(
            locationResponse => {
              if (this._MsgHandelService.handleSuccessResponse(locationResponse)) {
                this._MsgHandelService.showSuccessMsg(
                  '',
                  'Successfully updated the train!'
                );
                // this._Router.navigateByUrl('/locations');
                this.getTrains();
                this.getAllStations();
              }
            },
            error => {
              this._MsgHandelService.handleError(error);
            }
          );
        }
      });
      
    }

    deleteTrain(trainStation:any){
      let obj={"id":trainStation._id};
      swal({
        title: 'Are you sure?',
        text: 'Do you need to delete this train!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Yes',
        buttonsStyling: false
      }).then(result => {
        if (result.value) {
          this._InitService.deleteObj(obj).subscribe(
            data => {
              if (this._MsgHandelService.handleSuccessResponse(data)) {
                this._MsgHandelService.showSuccessMsg(
                  '',
                  'Train station successfully deleted !'
                );
                this.getTrains();
                 this.getAllStations();
              }
            },
            err => {
              this._MsgHandelService.handleError(err);
            }
          );
        }
      });
    
    }
}
