const trainStationModel = require('./trainStationModel');
const locationService = require('../location/locationService');

/**
 * find from the system
 */
module.exports.findLocations = (condition) => {
    return new Promise((resolve, reject) => {
        trainStationModel.find(condition).exec((err, location) => {
            err ? reject(err) : resolve(location);
        });
    })
};

module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        trainStationModel.find(condition).exec((err, location) => {
            err ? reject(err) : resolve(location);
        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        trainStationModel.findOneAndDelete({ _id: id },
            async (err, location) => {
                if (err)
                    reject(err);
                else if (location != null || location != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            // console.log(data);
            let item = new trainStationModel();
            if(data.location){
             
                let seletedLocation =await locationService.findOne(data.location);
                console.log("seletedLocation");
                console.log(seletedLocation);
                if(seletedLocation){
                    item.location = seletedLocation.location;
                    item.name = seletedLocation.name;
                }else{
                    item.name = data.name;
                }
                
            }else{
                item.name = data.name;
            }
           

            // console.log(item);
            item.save(async (error, data) => {
                if (error) {
                    console.log(error);
                    reject(error);
                    
                } else {
                    resolve(data);
                }
            })
        } catch (error) {
            reject('' + error)
        }
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        trainStationModel.findOne({ _id: id }).populate('location')
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = (id, data) => {
    return new Promise((resolve, reject) => {
        if(data.useLocationName){
            let seletedLocation = calocationService.findOne(data.id);
            if(seletedLocation){
                data.name = seletedLocation.name;
            }else{
                data.name = data.name;
            }
            
        }else{
            data.name = data.name;
        }
        trainStationModel.findByIdAndUpdate(id, data, { new: true })
            .exec((err, result) => {
                if (err)
                    reject(err)
                else if (result == null || result == undefined)
                    reject("Invalid id");
                else
                    resolve(result);
            });
    })
};
