(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-train-stations-trainStations-module"],{

/***/ "./src/app/modules/train-stations/filter.pipe.ts":
/*!*******************************************************!*\
  !*** ./src/app/modules/train-stations/filter.pipe.ts ***!
  \*******************************************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toLowerCase();
        return items.filter(function (it) {
            return it.name.toLowerCase().includes(searchText);
        });
    };
    FilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter'
        })
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/modules/train-stations/init/init.component.css":
/*!****************************************************************!*\
  !*** ./src/app/modules/train-stations/init/init.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modules/train-stations/init/init.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/modules/train-stations/init/init.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"col-md-12\">\r\n    <div class=\"card \">\r\n      <div class=\"card-header card-header-info card-header-icon\">\r\n        <div class=\"card-icon\">\r\n          <i class=\"material-icons\">build</i>\r\n          <h4 class=\"card-title\">Train Stations</h4>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body \">\r\n\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n            <div class=\"card\">\r\n              <div class=\"card-header\">\r\n                <form>\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                      <mat-form-field class=\"example-full-width\">\r\n                        <input matInput placeholder=\"Search\" type=\"text\"\r\n                        type=\"text\"\r\n                        [(ngModel)]=\"searchText\"\r\n                        [ngModelOptions]=\"{standalone: true}\">\r\n                      </mat-form-field>\r\n                    </div>\r\n                    <div class=\"col-md-6\">\r\n                      <button mat-mini-fab aria-label=\"Example icon-button with a heart icon\">\r\n                          <mat-icon \r\n                          matTooltip=\"View\"\r\n                        data-toggle=\"modal\"\r\n                        data-target=\"#myModal1\" \r\n                        (click)=\"changeState(null,false)\">add</mat-icon>\r\n                        </button>\r\n                  </div>\r\n\r\n                  </div>\r\n                </form>\r\n              </div>\r\n              <div class=\"card-body\">\r\n                <div class=\"table-responsive\">\r\n                  <table class=\"table table-hover\">\r\n                    <thead class=\"\">\r\n                      <!-- <th>\r\n                        ID\r\n                      </th>\r\n                      <th>\r\n                        Name\r\n                      </th>\r\n                      <th>\r\n                        Country\r\n                      </th> -->\r\n                      <th>\r\n                        Name\r\n                      </th>\r\n                      <th>\r\nLocation\r\n                      </th>\r\n                      <th>\r\n                        Action\r\n                      </th>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr  *ngFor=\"let trainStation of trainStations | filter : searchText\" >\r\n                        <td>\r\n                          <span *ngIf=\"trainStation.name\">{{trainStation.name}}</span> \r\n\r\n                        </td>\r\n                        <td>\r\n                           <span *ngIf=\"trainStation.location != null\">{{trainStation.location.name}}</span> \r\n                        </td>\r\n                     \r\n                        <td>\r\n                          <div class=\"row justify-content-start\">\r\n\r\n                            <i class=\"material-icons\" (click)=\"deleteStation(trainStation)\">\r\n                              delete\r\n                            </i>\r\n\r\n                            &nbsp;\r\n                            <i class=\"material-icons\"\r\n                            matTooltip=\"View\"\r\n                          data-toggle=\"modal\"\r\n                          data-target=\"#myModal1\"\r\n                          (click)=\"changeState(trainStation._id,true)\">\r\n                              edit\r\n                            </i>\r\n                            &nbsp;\r\n                            <i class=\"material-icons\">\r\n                              desktop_windows\r\n                            </i>\r\n\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n</div>\r\n<div\r\n  class=\"modal fade\"\r\n  id=\"myModal1\"\r\n  tabindex=\"-1\"\r\n  role=\"dialog\"\r\n  aria-labelledby=\"myModalLabel\"\r\n  aria-hidden=\"true\"\r\n>\r\n  <div class=\"modal-dialog\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"card-body modal-body\">\r\n          <div class=\"example-container\">\r\n            <form [formGroup]=\"trainStationForm\">\r\n                <mat-form-field>\r\n                    <mat-label>Location</mat-label>\r\n                    <mat-select formControlName=\"location\">\r\n                      <mat-option>None</mat-option>\r\n                      <mat-option *ngFor=\"let location of locations\" [value]=\"location._id\">{{location.name}}</mat-option>\r\n                    </mat-select>\r\n                  </mat-form-field>\r\n                  <div\r\n                  class=\"error-msg\"\r\n                  *ngIf=\"\r\n                    !trainStationForm.controls['location'].valid &&\r\n                    trainStationForm.controls['location'].touched\r\n                  \"\r\n                >\r\n                  <small>Location is <strong>required</strong> </small>\r\n                </div>\r\n              <mat-form-field>\r\n                <input matInput placeholder=\"Location Name\"  formControlName=\"locationName\">\r\n              </mat-form-field>\r\n            <span>\r\n                <div\r\n                class=\"error-msg\"\r\n                *ngIf=\"\r\n                  !trainStationForm.controls['locationName'].valid &&\r\n                  trainStationForm.controls['locationName'].touched\r\n                \"\r\n              >\r\n                <small>Location Name is <strong>required</strong> </small>\r\n              </div>\r\n            </span>\r\n              \r\n\r\n            </form>\r\n            </div>\r\n         \r\n              <button *ngIf=\"!isUpdate\"\r\n                mat-raised-button\r\n                type=\"submit\"\r\n                class=\"btn btn-primary pull-right\"\r\n                [disabled]=\"!trainStationForm.valid\"\r\n                (click)=\"createStation()\"\r\n              >\r\n                Save\r\n              </button>\r\n              <button *ngIf=\"isUpdate\"\r\n                mat-raised-button\r\n                type=\"submit\"\r\n                class=\"btn btn-primary pull-right\"\r\n                [disabled]=\"!trainStationForm.valid\"\r\n                (click)=\"updateStation()\"\r\n              >\r\n                Update\r\n              </button>\r\n          </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/modules/train-stations/init/init.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/modules/train-stations/init/init.component.ts ***!
  \***************************************************************/
/*! exports provided: InitComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitComponent", function() { return InitComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_init_init_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/init/init.service */ "./src/app/modules/train-stations/services/init/init.service.ts");
/* harmony import */ var src_app_commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/commons/services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InitComponent = /** @class */ (function () {
    function InitComponent(_FormBuilder, _InitService, _MsgHandelService) {
        this._FormBuilder = _FormBuilder;
        this._InitService = _InitService;
        this._MsgHandelService = _MsgHandelService;
        this.locations = [];
        this.trainStations = [];
        this.trainStationForm = this._FormBuilder.group({
            location: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])],
            locationName: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])]
        });
    }
    InitComponent.prototype.ngOnInit = function () {
        this.getLocations();
        this.getAllStations();
    };
    InitComponent.prototype.changeState = function (event, status) {
        console.log(event);
        this.isUpdate = status;
        this.updateId = event;
    };
    InitComponent.prototype.getLocations = function () {
        var _this = this;
        this._InitService.getAlLocations().subscribe(function (Response) {
            if (_this._MsgHandelService.handleSuccessResponse(Response)) {
                _this.locations = Response['data'];
                console.log(" this.allLocations");
                console.log(_this.locations);
            }
        }, function (error) {
            _this._MsgHandelService.handleError(error);
        });
    };
    InitComponent.prototype.getAllStations = function () {
        var _this = this;
        this._InitService.getAll().subscribe(function (Response) {
            console.log("Response this.trainStation");
            console.log(Response);
            if (_this._MsgHandelService.handleSuccessResponse(Response)) {
                _this.trainStations = Response['data'];
                console.log(" this.trainStation");
                console.log(_this.trainStations);
            }
        }, function (error) {
            _this._MsgHandelService.handleError(error);
        });
    };
    InitComponent.prototype.createStation = function () {
        var _this = this;
        console.log(this.trainStationForm.controls['location'].value);
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()({
            title: 'Are you sure?',
            text: 'Do you need save this train station!',
            type: 'info',
            showCancelButton: true,
            showLoaderOnConfirm: true
        }).then(function (result) {
            if (result.value) {
                var obj = {
                    "name": _this.trainStationForm.controls['locationName'].value,
                    "location": _this.trainStationForm.controls['location'].value
                };
                _this._InitService.postObj(obj).subscribe(function (locationResponse) {
                    if (_this._MsgHandelService.handleSuccessResponse(locationResponse)) {
                        _this._MsgHandelService.showSuccessMsg('', 'Successfully saved the location!');
                        // this._Router.navigateByUrl('/locations');
                        _this.getLocations();
                        _this.getAllStations();
                    }
                }, function (error) {
                    _this._MsgHandelService.handleError(error);
                });
            }
        });
    };
    InitComponent.prototype.updateStation = function () {
        var _this = this;
        console.log(this.trainStationForm.controls['location'].value);
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()({
            title: 'Are you sure?',
            text: 'Do you need update this train station!',
            type: 'info',
            showCancelButton: true,
            showLoaderOnConfirm: true
        }).then(function (result) {
            if (result.value) {
                var obj = {
                    "id": _this.updateId,
                    "name": _this.trainStationForm.controls['locationName'].value,
                    "location": _this.trainStationForm.controls['location'].value
                };
                _this._InitService.putObj(obj).subscribe(function (locationResponse) {
                    if (_this._MsgHandelService.handleSuccessResponse(locationResponse)) {
                        _this._MsgHandelService.showSuccessMsg('', 'Successfully updated the location!');
                        // this._Router.navigateByUrl('/locations');
                        _this.getLocations();
                        _this.getAllStations();
                    }
                }, function (error) {
                    _this._MsgHandelService.handleError(error);
                });
            }
        });
    };
    InitComponent.prototype.deleteStation = function (trainStation) {
        var _this = this;
        var obj = { "id": trainStation._id };
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()({
            title: 'Are you sure?',
            text: 'Do you need to delete this train station!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                _this._InitService.deleteObj(obj).subscribe(function (data) {
                    if (_this._MsgHandelService.handleSuccessResponse(data)) {
                        _this._MsgHandelService.showSuccessMsg('', 'Train station successfully deleted !');
                        _this.getLocations();
                        _this.getAllStations();
                    }
                }, function (err) {
                    _this._MsgHandelService.handleError(err);
                });
            }
        });
    };
    InitComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-init',
            template: __webpack_require__(/*! ./init.component.html */ "./src/app/modules/train-stations/init/init.component.html"),
            styles: [__webpack_require__(/*! ./init.component.css */ "./src/app/modules/train-stations/init/init.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _services_init_init_service__WEBPACK_IMPORTED_MODULE_1__["InitService"], src_app_commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_2__["MsgHandelService"]])
    ], InitComponent);
    return InitComponent;
}());



/***/ }),

/***/ "./src/app/modules/train-stations/services/init/init.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/modules/train-stations/services/init/init.service.ts ***!
  \**********************************************************************/
/*! exports provided: InitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitService", function() { return InitService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _infrastructure_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../infrastructure/api.service */ "./src/app/infrastructure/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import common service

var InitService = /** @class */ (function () {
    function InitService(_apiService) {
        this._apiService = _apiService;
        this.loggedUser = window.localStorage.getItem('userId');
    }
    InitService.prototype.getAlLocations = function () {
        return this._apiService.get("location/getAll");
    };
    InitService.prototype.getAll = function () {
        return this._apiService.get("trainStation");
    };
    InitService.prototype.getOne = function () {
        return this._apiService.get("api/feedback/getOne/" + this.loggedUser);
    };
    InitService.prototype.postObj = function (obj) {
        return this._apiService.post("trainStation/create", obj);
    };
    InitService.prototype.putObj = function (obj) {
        return this._apiService.post("trainStation/update", obj);
    };
    InitService.prototype.deleteObj = function (obj) {
        return this._apiService.post("trainStation/delete", obj);
    };
    InitService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_infrastructure_api_service__WEBPACK_IMPORTED_MODULE_1__["MainService"]])
    ], InitService);
    return InitService;
}());



/***/ }),

/***/ "./src/app/modules/train-stations/trainStations.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/modules/train-stations/trainStations.module.ts ***!
  \****************************************************************/
/*! exports provided: TrainStationsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrainStationsModule", function() { return TrainStationsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _app_material_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../app.material.module */ "./src/app/app.material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _trainStations_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./trainStations.routing */ "./src/app/modules/train-stations/trainStations.routing.ts");
/* harmony import */ var _services_init_init_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/init/init.service */ "./src/app/modules/train-stations/services/init/init.service.ts");
/* harmony import */ var _init_init_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./init/init.component */ "./src/app/modules/train-stations/init/init.component.ts");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./filter.pipe */ "./src/app/modules/train-stations/filter.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






/* services */

/* components */


var TrainStationsModule = /** @class */ (function () {
    function TrainStationsModule() {
    }
    TrainStationsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _app_material_module__WEBPACK_IMPORTED_MODULE_3__["AppMaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_trainStations_routing__WEBPACK_IMPORTED_MODULE_5__["_Routes"])
            ],
            declarations: [_init_init_component__WEBPACK_IMPORTED_MODULE_7__["InitComponent"], _filter_pipe__WEBPACK_IMPORTED_MODULE_8__["FilterPipe"]],
            providers: [_services_init_init_service__WEBPACK_IMPORTED_MODULE_6__["InitService"]]
        })
    ], TrainStationsModule);
    return TrainStationsModule;
}());



/***/ }),

/***/ "./src/app/modules/train-stations/trainStations.routing.ts":
/*!*****************************************************************!*\
  !*** ./src/app/modules/train-stations/trainStations.routing.ts ***!
  \*****************************************************************/
/*! exports provided: _Routes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_Routes", function() { return _Routes; });
/* harmony import */ var _init_init_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./init/init.component */ "./src/app/modules/train-stations/init/init.component.ts");
// component import

var _Routes = [
    {
        path: "",
        component: _init_init_component__WEBPACK_IMPORTED_MODULE_0__["InitComponent"]
    }
];


/***/ })

}]);
//# sourceMappingURL=modules-train-stations-trainStations-module.js.map