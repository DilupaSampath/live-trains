import { Component, OnInit, ViewChild, ElementRef ,EventEmitter, Input, Output, HostListener} from '@angular/core';
import { EChartOption } from 'echarts';
import { SocketService } from '../socket.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InitService } from '../services/init/init.service';
import { Snake } from './snake';
import { BestScoreManager } from '../game.storage.service';
import { Direction } from './direction';
import { Egg } from './egg';
import { AgmMap, LatLngBounds } from '@agm/core';
declare var google: any;
interface marker {
	lat: number;
	lng: number;
	label?: string;
  draggable: boolean;
  icon:string
}
@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {
  markers: marker[];
  readonly size = 20;
  readonly gridSize = this.size * this.size;
  readonly cellWidth = 10; // in px
  readonly cells = new Array(this.size * this.size);
  readonly timestep = 100;
  readonly ngStyleCells = {
    width: `${this.size * this.cellWidth}px`
  }
  readonly snake: Snake = new Snake();
  readonly direnum = Direction;
  dead = false;

  time = 0;

  egg: Egg = new Egg(this.gridSize);

  paused = false;




  validatePointTatus:boolean;
  realTimeDataResponce:any;
  realTimeDataForm: FormGroup;
  socketValidateMessage:any;
  messageSuccess :boolean;
  radioStatus:Number;
  announcementSubs: Subscription;
  announcementSubsValidate: Subscription;
  
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext') addresstext: any;
  @ViewChild('addresstext1') addresstext1: any;
  @ViewChild('AgmMap') agmMap: AgmMap;
  validateLocationForm: FormGroup;
  location:any;
  autocompleteInput: string;
  autocompleteInput2: string;
  queryWait: boolean;
    
  
  origin:any;
  destination:any;
  panelOpenState = false;
  lat: any = 7.8731;
  lng: any = 80.7718;
  zoom: Number = 7.5;
  saleStoreArray: Array<any> = [];

  styles: any;
  mani_store_mapIcon: any = {
    url: 'assets/img/main_sale.svg',
    scaledSize: {
      width: 35,
      height: 55
    }
  };

  store_mapIcon: any = {
    url: 'assets/img/sale.svg',
    scaledSize: {
      width: 30,
      height: 50
    }
  };

  @ViewChild('search')
  public searchElementRef: ElementRef;
  trendingVehicle: any;
  trendingBrand: any;
  trendingDealer: any;
  unRegVehicleCount: any = 0;
  dataDailySalesChart: any;
  dataWebsiteViewsChart: any;

  popularData: any = {
    labels: [],
    series: [],
    icons: []
  };

  chartOption1: EChartOption = {};

  chartOption2: EChartOption = {};

  chartOption3: EChartOption = {};

  chartOption4: EChartOption = {};

public travelMode: string = 'TRANSIT'
public transitOptions: any = {
    modes: ['TRAIN'],
}
realTimeTableData:any;
dir = undefined;
  constructor(private bestScoreService: BestScoreManager,private _FormBuilder: FormBuilder,  private _SocketService: SocketService,private _InitService:InitService) { 
    this.validatePointTatus=null;
    this.validateLocationForm = this._FormBuilder.group({
      lat: [null, Validators.compose([Validators.required])],
      lng: [null, Validators.compose([Validators.required])]
    });
    this.realTimeDataForm = this._FormBuilder.group({
      first_location_lat: [null, Validators.compose([Validators.required])],
      first_location_lon: [null, Validators.compose([Validators.required])],
      second_location_lat: [null, Validators.compose([Validators.required])],
      second_location_lon: [null, Validators.compose([Validators.required])],
      start_time: [null, Validators.compose([Validators.required])]
    });
  }
  ngAfterViewInit() {
    
    console.log(this.agmMap);
    this.agmMap.mapReady.subscribe(map => {
      const bounds: LatLngBounds = new google.maps.LatLngBounds();
      for (const mm of this.markers) {
        bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
      }
      map.fitBounds(bounds);
    });
  
    this.getPlaceAutocomplete();
    this.getPlaceAutocomplete2();
}
  ngOnInit() {

    this.doSpawnEgg();
    const runTime = () => {
      setTimeout(() => {
        this.goStep();
        this.dead = this.snake.checkDead();
        this.time++;
        if (!this.dead) {
          runTime();
        }
      }, this.timestep)
    }
    runTime();
    this.validatePointTatus=null;
    this.messageSuccess=false;
    this.radioStatus=1;
    this.getDirection();
       // call for listening the data
       this.announcementSubs = this._SocketService
       .getSocketAllMainValvesData()
       .subscribe(socket_response => {
         console.log(socket_response);
         this.realTimeTableData=socket_response;
       });
       this.announcementSubsValidate = this._SocketService
       .getSocketValidateData()
       .subscribe(socket_response => {
         console.log(socket_response);
       });
  }
  
  doTogglePause() {
    this.paused = !this.paused;
  }

  doSpawnEgg() {
    this.egg = new Egg(this.gridSize, this.snake);
  }

  goStep() {
    this.snake.goStep(this.size);
    this.eatEgg();
  }

  eatEgg() {
    const pos = this.snake.head.pos;
    if (this.isEgg(pos)) {
      this.doSpawnEgg();
      this.snake.grow();
    }
  }

  @HostListener('window:keydown', ['$event'])
  onKeypress(e: KeyboardEvent) {
    if (!this.dead) {
      const dir = KeyCodes[e.keyCode];
      this.changeDirAndGoStep(dir);
    }
  }

  changeDirAndGoStep(dir) {
    if (dir) {
      const canChangeDir = this.getCanChangeDir(dir, this.snake.dir);
      if (canChangeDir) {
        this.snake.dir = dir;
        this.goStep();
      }
    }
  }

  getCanChangeDir(d1: Direction, d2: Direction) {
    const dirs = [d1, d2];
    const filteredUpDown = dirs.filter(dir => dir === Direction.UP || dir === Direction.DOWN).length;
    const onlyOneDir = filteredUpDown === 2 || filteredUpDown === 0;
    return !onlyOneDir;
  }

  isEgg(cell) {
    return this.egg.pos === cell;
  }

  ngStyleCell(idx: number) {
    const bgEgg = this.isEgg(idx) ? 'orange' : null;
    const bgSnake = this.snake.isSnakeCell(idx) ? 'red' : null;
    const defaultBg = '#ccc';
    return {
      width: `${this.cellWidth}px`,
      height: `${this.cellWidth}px`,
      background: bgEgg || bgSnake || defaultBg
    }
  }
  emitRealTimeData(){
    console.log("act");
    let data={
      lat:this.validateLocationForm.controls['lat'].value,
      lng:this.validateLocationForm.controls['lng'].value,
    };
    this._SocketService.sendValidateSocket(data);
    this.messageSuccess = true;

setTimeout(()=>{    //<<<---    using ()=> syntax
      this.messageSuccess = false;
 }, 1000);

  }
  validatePointRequest(){
    console.log(new Date().setSeconds(new Date().getSeconds() -this.realTimeDataForm.controls['start_time'].value));
        var obj={
          "id":"5d594e03b789ed3ca4202605",
          "point": { 
            "lon": Number(this.validateLocationForm.controls['lat'].value), 
            "lat": Number(this.validateLocationForm.controls['lng'].value), 
          }
           }
          
          
        setTimeout(()=>{    //<<<---    using ()=> syntax
          this.messageSuccess = false;
     }, 1000);
    
    this._InitService.validatePoint(obj).subscribe(responce=>{
      console.log(responce);
      this.validatePointTatus=responce.data.validateStatus;
    });
      }
  sendRealTimeDataRequest(){
console.log(new Date().setSeconds(new Date().getSeconds() -this.realTimeDataForm.controls['start_time'].value));
    var obj={
      "firstLat":this.realTimeDataForm.controls['first_location_lat'].value,
      "firstLng":this.realTimeDataForm.controls['first_location_lon'].value,
      "firstTime": new Date().setSeconds(new Date().getSeconds() -this.realTimeDataForm.controls['start_time'].value),
      "secondLat":this.realTimeDataForm.controls['second_location_lat'].value,
      "secondLng":this.realTimeDataForm.controls['second_location_lon'].value,
      "secondTime":new Date().getTime(),
    }
    setTimeout(()=>{    //<<<---    using ()=> syntax
      this.messageSuccess = false;
 }, 1000);

this._InitService.postObjArrvalTime(obj).subscribe(responce=>{
  console.log(responce);
  this.realTimeDataResponce=responce;
});
  }
  nearByPlaceRequest(){
   this._InitService.postObjNearByPlaceMapRequest(this.location).subscribe(responce=>{
    this.markers=responce.data;
   });
    // this.markers
  }
  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
        {
            componentRestrictions: { country: 'LK' },
            types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
        });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        this.invokeEvent(place);
    });
  }
  private getPlaceAutocomplete2() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext1.nativeElement,
        {
            componentRestrictions: { country: 'LK' },
            types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
        });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        this.invokeEvent(place);
    });
  }
  
invokeEvent(place: any) {
  this.location={
    "lat":place.geometry.location.lat(),
    "lang":place.geometry.location.lng()
  }
  console.log(this.location);
  
  this.setAddress.emit(place);
}

public getDirection() {
  this.dir = {
    origin: { lat: 6.937432, lng: 79.879112 },
    destination: { lat: 9.665077, lng: 80.020654 }
  }
}
  
  // getDirection() {
  //   // this.origin = { lat: 6.937432, lng: 79.879112 };
  //   // this.destination = { lat: 9.665077, lng: 80.020654 };
  //  this.dir = {
  //   origin: { lat: 6.937432, lng: 79.879112 },
  //   destination: { lat: 9.665077, lng: 80.020654 }
  // }
  //   // this.origin = 'Taipei Main Station';
  //   // this.destination = 'Taiwan Presidential Office';
  // }
  ngOnDestroy(): void {
    if (this.announcementSubs !== undefined && this.announcementSubs !== null) {
      this.announcementSubs.unsubscribe();
    }
  }



}

const KeyCodes = {
  37: Direction.LEFT,
  38: Direction.UP,
  39: Direction.RIGHT,
  40: Direction.DOWN
}