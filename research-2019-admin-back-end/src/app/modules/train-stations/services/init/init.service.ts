import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.localStorage.getItem('userId');
  
  getAlLocations(): Observable<any> {
    return this._apiService.get(`location/getAll`);
  }
  getAll(): Observable<any> {
    return this._apiService.get(`trainStation`);
  }

  getOne(): Observable<any> {
    return this._apiService.get(`api/feedback/getOne/${this.loggedUser}`);
  }

  postObj(obj): Observable<any> {
    return this._apiService.post(`trainStation/create`, obj);
  }

  putObj(obj): Observable<any> {
    return this._apiService.post(`trainStation/update`, obj);
  }

  deleteObj(obj): Observable<any> {
    
    return this._apiService.post(`trainStation/delete`, obj);
  }
}
