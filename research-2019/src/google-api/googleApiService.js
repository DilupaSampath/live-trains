const dateService = require('../../services/dateService');
const googleApiService = require('../../services/google-api-service');


/**
 * find valve from the system
 */
module.exports.getDistance = (data) => {
    return new Promise((resolve, reject) => {
        console.log("***********service");

       resolve( googleApiService.getDistance(data));
    })
};

module.exports.getNearByPlaces = (data) => {
    return new Promise((resolve, reject) => {
     googleApiService.makeNearByPlaceRequestTrainFilter(data,function (err, nearByPlaces) {
            if (!err){
              return resolve(nearByPlaces);
            }else{
                return  reject(err); 
            }
                
        } 
     )
});
}
/**
 * find valve from the system
 */
module.exports.getDataWithArrivelTime = (data) => {
    return new Promise((resolve, reject) => {
       try {
        resolve( googleApiService.getDataWithArrivelTime(data));
       } catch (error) {
           reject(googleApiService.getDataWithArrivelTime(data));
       } 
    })
};

/**
 * find valve from the system
 */
module.exports.getSpeed = (data) => {
    return new Promise((resolve, reject) => {
       try {
        resolve( googleApiService.getSpeed(data));
       } catch (error) {
           reject(googleApiService.getSpeed(data));
       } 
    })
};