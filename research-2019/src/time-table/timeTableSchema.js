const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  name: joi.string().required(),
  current_location:joi.object().keys({
    type:  joi.string().required(),
    coordinates: joi.array().items(
      joi.number().required()
  ),
  }),
  start:joi.string().alphanum().min(24).max(24),
  end:joi.string().alphanum().min(24).max(24),
  train:joi.string().alphanum().min(24).max(24),
  distance:joi.number(),
  isRunning:joi.boolean(),
  start_time:joi.number().required(),
  end_time:joi.number().required(),
  short_start_time:joi.string().required(),
  short_end_time:joi.string().required(),
  
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  id: joi.string().alphanum().min(24).max(24),
  name: joi.string(),
  current_location:joi.object().keys({
    type:  joi.string().required(),
    coordinates: joi.array().items(
      joi.number().required()
  ),
  }),
  start:joi.string().alphanum().min(24).max(24),
  end:joi.string().alphanum().min(24).max(24),
  distance:joi.number(),
  isRunning:joi.boolean(),
  start_time:joi.number(),
  end_time:joi.number(),
  short_start_time:joi.string().required(),
  short_end_time:joi.string().required(),
});