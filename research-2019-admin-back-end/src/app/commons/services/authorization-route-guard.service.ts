import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot
} from '@angular/router';

/**
 * import services
 */

import { JwtService } from '../../infrastructure/jwt.service';
import { JwtTokenValidatorService } from './jwt-token-validator.service';
import { MsgHandelService } from '../../commons/services/msg-handel.service';

@Injectable({
  providedIn: 'root'
})
export class AuthRouteGuardService {
  constructor(
    private _Router: Router,
    private _MsgHandelService: MsgHandelService,
    private _JwtTokenValidatorService: JwtTokenValidatorService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // get all routes
    const routes = this._JwtTokenValidatorService.getAuthorizedRoutes();

    let url = state.url;

    // filter URL and remove query parameters
    if (state.url.includes('?')) {
      url = state.url.split('?')[0];
    }

    if (routes !== null) {
      if (routes.includes(url)) {
        return true;
      }
    }

    this._MsgHandelService.showErrorMsg(
      'Unauthorized',
      'You are not allow to access this route'
    );
    return false;
  }
}
