import { Routes } from '@angular/router';

// import components
import { DashboardComponent } from './dashboard.component';

// import router guard
import { AuthRouteGuardService } from '../../commons/services/authorization-route-guard.service';

export const DashboardRoutes: Routes = [
  {
    path: '',
    canActivate: [],
    children: [
      {
        path: '',
        canActivate: [],
        component: DashboardComponent
      }
    ]
  }
];
