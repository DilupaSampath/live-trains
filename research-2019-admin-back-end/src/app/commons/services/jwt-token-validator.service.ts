import { Injectable } from '@angular/core';

// import jwt token decoder
import * as jwt_decode from 'jwt-decode';
// import local storage handle service
import { LocalStorageHandleService } from './localStorageHandle.service';
// message handle service
import { MsgHandelService } from './msg-handel.service';

@Injectable({
  providedIn: 'root'
})
export class JwtTokenValidatorService {
  constructor(
    private _LocalStorageHandleService: LocalStorageHandleService,
    private _MsgHandelService: MsgHandelService
  ) {}

  public validateUserRole(userRole: string) {
    try {
      const tokenBody = jwt_decode(
        this._LocalStorageHandleService.getItem('token')
      );
      // console.log(tokenBody.role);

      if (userRole === tokenBody.role) {
        return true;
      } else {
        return false;
      }
    } catch (Error) {
      this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
      return false;
    }
  }

  public getLoggedUserId() {
    try {
      const tokenBody = jwt_decode(
        this._LocalStorageHandleService.getItem('token')
      );
      return tokenBody['_id'] === undefined ? null : tokenBody['_id'];
    } catch (Error) {
      this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
      return null;
    }
  }

  public accessUser_Id() {
    try {
      const tokenBody = jwt_decode(
        this._LocalStorageHandleService.getItem('token')
      );
      return tokenBody['_id'] === undefined ? null : tokenBody['_id'];
    } catch (Error) {
      this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
      return null;
    }
  }

  public getLoggedUserName() {
    try {
      const tokenBody = jwt_decode(
        this._LocalStorageHandleService.getItem('token')
      );
      return tokenBody['user_name'] === undefined
        ? null
        : tokenBody['user_name'];
    } catch (Error) {
      this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
      return null;
    }
  }

  public getAuthorizedRoutes() {
    try {
      const tokenBody = jwt_decode(
        this._LocalStorageHandleService.getItem('token')
      );
      return tokenBody['routs'] === undefined ? null : tokenBody['routs'];
    } catch (Error) {
      this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
      return null;
    }
  }
}
