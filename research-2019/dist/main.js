(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./modules/dashboard/dashboard.module": [
		"./src/app/modules/dashboard/dashboard.module.ts",
		"modules-dashboard-dashboard-module"
	],
	"./modules/example/custom.module": [
		"./src/app/modules/example/custom.module.ts",
		"modules-example-custom-module"
	],
	"./modules/time-tables/timeTables.module": [
		"./src/app/modules/time-tables/timeTables.module.ts",
		"modules-time-tables-timeTables-module"
	],
	"./modules/train-stations/trainStations.module": [
		"./src/app/modules/train-stations/trainStations.module.ts",
		"modules-train-stations-trainStations-module"
	],
	"./modules/trains/trains.module": [
		"./src/app/modules/trains/trains.module.ts",
		"modules-trains-trains-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error('Cannot find module "' + req + '".');
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            var body = document.getElementsByTagName('body')[0];
            var modalBackdrop = document.getElementsByClassName('modal-backdrop')[0];
            if (body.classList.contains('modal-open')) {
                body.classList.remove('modal-open');
                modalBackdrop.remove();
            }
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-my-app',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.material.module.ts":
/*!****************************************!*\
  !*** ./src/app/app.material.module.ts ***!
  \****************************************/
/*! exports provided: AppMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMaterialModule", function() { return AppMaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material-moment-adapter */ "./node_modules/@angular/material-moment-adapter/esm5/material-moment-adapter.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/* Angular material modules used in the project */


var AppMaterialModule = /** @class */ (function () {
    function AppMaterialModule() {
    }
    AppMaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_1__["MatMomentDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_1__["MatMomentDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"]
            ]
        })
    ], AppMaterialModule);
    return AppMaterialModule;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _app_material_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.material.module */ "./src/app/app.material.module.ts");
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-lightbox */ "./node_modules/ngx-lightbox/index.js");
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(ngx_lightbox__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _commons_components_footer_footer_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./commons/components/footer/footer.module */ "./src/app/commons/components/footer/footer.module.ts");
/* harmony import */ var _commons_components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./commons/components/navbar/navbar.module */ "./src/app/commons/components/navbar/navbar.module.ts");
/* harmony import */ var _commons_components_sidebar_sidebar_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./commons/components/sidebar/sidebar.module */ "./src/app/commons/components/sidebar/sidebar.module.ts");
/* harmony import */ var angular2_image_upload__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! angular2-image-upload */ "./node_modules/angular2-image-upload/fesm5/angular2-image-upload.js");
/* harmony import */ var _infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./infrastructure/jwt.service */ "./src/app/infrastructure/jwt.service.ts");
/* harmony import */ var _infrastructure_api_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./infrastructure/api.service */ "./src/app/infrastructure/api.service.ts");
/* harmony import */ var _commons_services_authentication_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./commons/services/authentication.service */ "./src/app/commons/services/authentication.service.ts");
/* harmony import */ var _infrastructure_auth_guard_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./infrastructure/auth-guard.service */ "./src/app/infrastructure/auth-guard.service.ts");
/* harmony import */ var _commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./commons/services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
/* harmony import */ var _commons_services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./commons/services/jwt-token-validator.service */ "./src/app/commons/services/jwt-token-validator.service.ts");
/* harmony import */ var _commons_services_authorization_route_guard_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./commons/services/authorization-route-guard.service */ "./src/app/commons/services/authorization-route-guard.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _commons_components_login_login_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./commons/components/login/login.component */ "./src/app/commons/components/login/login.component.ts");
/* harmony import */ var _commons_layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./commons/layouts/admin/admin-layout.component */ "./src/app/commons/layouts/admin/admin-layout.component.ts");
/* harmony import */ var _commons_layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./commons/layouts/auth/auth-layout.component */ "./src/app/commons/layouts/auth/auth-layout.component.ts");
/* harmony import */ var _commons_components_register_register_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./commons/components/register/register.component */ "./src/app/commons/components/register/register.component.ts");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./filter.pipe */ "./src/app/filter.pipe.ts");
/* harmony import */ var _commons_pipes_year_month_pipe__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./commons/pipes/year-month.pipe */ "./src/app/commons/pipes/year-month.pipe.ts");
/* harmony import */ var _commons_pipes_order_by_pipe__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./commons/pipes/order-by.pipe */ "./src/app/commons/pipes/order-by.pipe.ts");
/* harmony import */ var _commons_pipes_date_difference_pipe__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./commons/pipes/date-difference.pipe */ "./src/app/commons/pipes/date-difference.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* Built-in Modules */








/**
 * Custom Modules
 */



 // import light box



 // image upload library
/**
 * common services
 */







/**
 * common Components
 */









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_http__WEBPACK_IMPORTED_MODULE_1__["HttpModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _commons_components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_12__["NavbarModule"],
                _commons_components_footer_footer_module__WEBPACK_IMPORTED_MODULE_11__["FooterModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _commons_components_sidebar_sidebar_module__WEBPACK_IMPORTED_MODULE_13__["SidebarModule"],
                ngx_lightbox__WEBPACK_IMPORTED_MODULE_10__["LightboxModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _app_material_module__WEBPACK_IMPORTED_MODULE_9__["AppMaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrModule"].forRoot(),
                angular2_image_upload__WEBPACK_IMPORTED_MODULE_14__["ImageUploadModule"].forRoot(),
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(_app_routing__WEBPACK_IMPORTED_MODULE_8__["AppRoutes"])
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_22__["AppComponent"],
                _commons_components_login_login_component__WEBPACK_IMPORTED_MODULE_23__["LoginComponent"],
                _commons_components_register_register_component__WEBPACK_IMPORTED_MODULE_26__["RegisterComponent"],
                _commons_layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_25__["AuthLayoutComponent"],
                _commons_layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_24__["AdminLayoutComponent"],
                _filter_pipe__WEBPACK_IMPORTED_MODULE_27__["FilterPipe"],
                _commons_pipes_date_difference_pipe__WEBPACK_IMPORTED_MODULE_30__["DateDifferencePipe"],
                _commons_pipes_order_by_pipe__WEBPACK_IMPORTED_MODULE_29__["OrderByPipe"],
                _commons_pipes_year_month_pipe__WEBPACK_IMPORTED_MODULE_28__["YearMonthPipe"]
            ],
            providers: [
                _infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_15__["JwtService"],
                _infrastructure_api_service__WEBPACK_IMPORTED_MODULE_16__["MainService"],
                _commons_services_authentication_service__WEBPACK_IMPORTED_MODULE_17__["AuthService"],
                _commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_19__["MsgHandelService"],
                _infrastructure_auth_guard_service__WEBPACK_IMPORTED_MODULE_18__["AuthGuardService"],
                _commons_services_authorization_route_guard_service__WEBPACK_IMPORTED_MODULE_21__["AuthRouteGuardService"],
                _commons_services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_20__["JwtTokenValidatorService"],
                {
                    provide: _angular_common__WEBPACK_IMPORTED_MODULE_3__["LocationStrategy"],
                    useClass: _angular_common__WEBPACK_IMPORTED_MODULE_3__["HashLocationStrategy"]
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_22__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: AppRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutes", function() { return AppRoutes; });
/* harmony import */ var _commons_components_login_login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./commons/components/login/login.component */ "./src/app/commons/components/login/login.component.ts");
/* harmony import */ var _commons_components_register_register_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./commons/components/register/register.component */ "./src/app/commons/components/register/register.component.ts");
/* harmony import */ var _commons_layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./commons/layouts/admin/admin-layout.component */ "./src/app/commons/layouts/admin/admin-layout.component.ts");
/* components */



/* Router List */
var AppRoutes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: _commons_components_login_login_component__WEBPACK_IMPORTED_MODULE_0__["LoginComponent"] },
    { path: 'register', component: _commons_components_register_register_component__WEBPACK_IMPORTED_MODULE_1__["RegisterComponent"] },
    // when after login to the system at the first time , this route will be called
    {
        path: '',
        redirectTo: 'dashboard',
        canActivate: [],
        pathMatch: 'full'
    },
    // lazy loaded modules
    {
        path: '',
        component: _commons_layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_2__["AdminLayoutComponent"],
        canActivate: [],
        children: [
            {
                path: 'dashboard',
                canActivate: [],
                loadChildren: './modules/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'locations',
                canActivate: [],
                loadChildren: './modules/example/custom.module#CustomModule'
            },
            {
                path: 'train-stations',
                canActivate: [],
                loadChildren: './modules/train-stations/trainStations.module#TrainStationsModule'
            },
            {
                path: 'trains',
                canActivate: [],
                loadChildren: './modules/trains/trains.module#TrainsModule'
            },
            {
                path: 'time-tables',
                canActivate: [],
                loadChildren: './modules/time-tables/timeTables.module#timeTablesModule'
            }
        ]
    }
];


/***/ }),

/***/ "./src/app/commons/components/footer/footer.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/commons/components/footer/footer.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer \">\r\n  <div class=\"container\">\r\n    <nav class=\"pull-left\">\r\n      <ul>\r\n        <li class=\"cursor \">\r\n          <a\r\n            [class]=\"\r\n              loginStatus === true\r\n                ? 'custom_text_loggedIn'\r\n                : 'custom_text_loggedOut'\r\n            \"\r\n            target=\"_blank\"\r\n          >\r\n         Live Trains\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </nav>\r\n    <div class=\"copyright pull-right\">\r\n      <p\r\n        [class]=\"\r\n          loginStatus === true\r\n            ? 'custom_text_loggedIn'\r\n            : 'custom_text_loggedOut'\r\n        \"\r\n        href=\"#\"\r\n      >\r\n        Copyright &copy; {{ test | date: 'yyyy' }} SperaLabs All Rights\r\n        Reserved.\r\n      </p>\r\n    </div>\r\n  </div>\r\n</footer>\r\n"

/***/ }),

/***/ "./src/app/commons/components/footer/footer.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/commons/components/footer/footer.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cursor {\n  cursor: pointer; }\n\n.custom_text_loggedIn {\n  color: #3c4858 !important; }\n\n.custom_text_loggedOut {\n  color: white !important; }\n\n.custom_text_loggedOut:hover {\n  color: white !important; }\n\n.custom_text_loggedIn:hover {\n  color: #3c4858 !important; }\n"

/***/ }),

/***/ "./src/app/commons/components/footer/footer.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/commons/components/footer/footer.component.ts ***!
  \***************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../infrastructure/jwt.service */ "./src/app/infrastructure/jwt.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * import service
 */

var FooterComponent = /** @class */ (function () {
    function FooterComponent(_JwtService) {
        this._JwtService = _JwtService;
        this.test = new Date();
        this.loginStatus = false;
        this.loginStatus = false;
    }
    FooterComponent.prototype.ngOnInit = function () {
        if (this._JwtService.getToken()) {
            this.loginStatus = true;
        }
        else {
            this.loginStatus = false;
        }
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer-cmp',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/commons/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/commons/components/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [_infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_1__["JwtService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/commons/components/footer/footer.module.ts":
/*!************************************************************!*\
  !*** ./src/app/commons/components/footer/footer.module.ts ***!
  \************************************************************/
/*! exports provided: FooterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterModule", function() { return FooterModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer.component */ "./src/app/commons/components/footer/footer.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FooterModule = /** @class */ (function () {
    function FooterModule() {
    }
    FooterModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]],
            exports: [_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]]
        })
    ], FooterModule);
    return FooterModule;
}());



/***/ }),

/***/ "./src/app/commons/components/login/login.component.html":
/*!***************************************************************!*\
  !*** ./src/app/commons/components/login/login.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page \">\r\n  <div class=\"page-header login-page header-filter\" filter-color=\"blue\">\r\n    <div class=\"container\">\r\n      <div class=\"col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto\">\r\n        <form\r\n          class=\"form\"\r\n          [formGroup]=\"rForm\"\r\n          (ngSubmit)=\"userLogin(rForm.value)\"\r\n        >\r\n          <div class=\"card card-login card-hidden\">\r\n            <div class=\"card-header card-header-info text-center pb-3\">\r\n              <h4 class=\"card-title \">Live Trains Admin Login</h4>\r\n            </div>\r\n            <div class=\"card-body \">\r\n              <span class=\"bmd-form-group\">\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\">\r\n                      <i class=\"material-icons\"> email</i>\r\n                    </span>\r\n                  </div>\r\n                  <input\r\n                    type=\"text\"\r\n                    class=\"form-control\"\r\n                    placeholder=\"Email\"\r\n                    formControlName=\"username\"\r\n                  />\r\n                </div>\r\n                <div\r\n                  class=\"error-msg\"\r\n                  *ngIf=\"\r\n                    !rForm.controls['username'].valid &&\r\n                    rForm.controls['username'].touched\r\n                  \"\r\n                >\r\n                  <small>Valid Email is <strong>required</strong> </small>\r\n                </div>\r\n              </span>\r\n              <span class=\"bmd-form-group\">\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\">\r\n                      <i class=\"material-icons\">lock_outline</i>\r\n                    </span>\r\n                  </div>\r\n                  <input\r\n                    type=\"password\"\r\n                    class=\"form-control\"\r\n                    placeholder=\"Password\"\r\n                    formControlName=\"password\"\r\n                  />\r\n                </div>\r\n                <div\r\n                  class=\"error-msg\"\r\n                  *ngIf=\"\r\n                    !rForm.controls['password'].valid &&\r\n                    rForm.controls['password'].touched\r\n                  \"\r\n                >\r\n                  <small>Password is <strong> required </strong> </small>\r\n                </div>\r\n              </span>\r\n            </div>\r\n            <div class=\"card-footer justify-content-center\">\r\n              <a\r\n                href=\"javascript:void(0)\"\r\n                class=\"btn btn-info btn-link\"\r\n                [routerLink]=\"['/register']\"\r\n                >Sign Up</a\r\n              >\r\n              <button\r\n                href=\"javascript:void(0)\"\r\n                type=\"submit\"\r\n                class=\"btn btn-info \"\r\n                [disabled]=\"!rForm.valid\"\r\n              >\r\n                Login\r\n              </button>\r\n            </div>\r\n            <div\r\n              class=\"col-md-12 d-flex justify-content-center py-2\"\r\n              *ngIf=\"rForm.controls['username'].valid\"\r\n            >\r\n              <a\r\n                (click)=\"forgetPassword()\"\r\n                style=\"color: #5a5555; font-style:unset;cursor: pointer;\"\r\n                >Forget password ?</a\r\n              >\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n    <app-footer-cmp class=\"footer-display\"></app-footer-cmp>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/commons/components/login/login.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/commons/components/login/login.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-button-color {\n  background: linear-gradient(60deg, #0f1018, #576ca3) !important; }\n\n.error-msg {\n  margin-top: -5px;\n  margin-left: 50px;\n  color: red; }\n\n.wrapper-full-page {\n  background: url('background.jpg');\n  background-repeat: no-repeat;\n  background-size: cover; }\n\n.footer-display {\n  display: block; }\n\n@media only screen and (max-width: 600px) {\n  .footer-display {\n    display: none; } }\n"

/***/ }),

/***/ "./src/app/commons/components/login/login.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/commons/components/login/login.component.ts ***!
  \*************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/commons/services/authentication.service.ts");
/* harmony import */ var _infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../infrastructure/jwt.service */ "./src/app/infrastructure/jwt.service.ts");
/* harmony import */ var _services_localStorageHandle_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/localStorageHandle.service */ "./src/app/commons/services/localStorageHandle.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../commons/services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
/* harmony import */ var _services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/jwt-token-validator.service */ "./src/app/commons/services/jwt-token-validator.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 *  import service
 */



// import form modules

/**
 * import common services
 */


var LoginComponent = /** @class */ (function () {
    function LoginComponent(_MsgHandelService, _ElementRef, _FormBuilder, _Router, _AuthService, _JwtService, _JwtTokenValidatorService, _LocalStorageHandleService) {
        this._MsgHandelService = _MsgHandelService;
        this._ElementRef = _ElementRef;
        this._FormBuilder = _FormBuilder;
        this._Router = _Router;
        this._AuthService = _AuthService;
        this._JwtService = _JwtService;
        this._JwtTokenValidatorService = _JwtTokenValidatorService;
        this._LocalStorageHandleService = _LocalStorageHandleService;
        this.test = new Date();
        this.user = {};
        this.nativeElement = _ElementRef.nativeElement;
        this.sidebarVisible = false;
        // form validation
        this.rForm = this._FormBuilder.group({
            username: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].email])
            ],
            password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required])]
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        /**
         * redirect to dashboard if the user is already logged in
         */
        if (this._JwtService.getToken()) {
            this._Router.navigateByUrl('/dashboard');
        }
        this.loading_gif = document.getElementById('loading-div');
        this.loading_info = document.getElementById('loading-info');
        var navbar = this._ElementRef.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        var card = document.getElementsByClassName('card')[0];
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
    };
    LoginComponent.prototype.sidebarToggle = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible === false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        }
        else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    };
    LoginComponent.prototype.forgetPassword = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()({
            title: 'Are you sure?',
            text: 'Your current password will be reset from this action',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function (willDelete) {
            if (willDelete.value) {
                _this._AuthService
                    .forgerPassword({ user_name: _this.rForm.controls['username'].value })
                    .subscribe(function (response) {
                    if (_this._MsgHandelService.handleSuccessResponse(response)) {
                        _this._MsgHandelService.showSuccessMsg('', 'Please check your email!');
                    }
                }, function (error) {
                    _this._MsgHandelService.handleError(error);
                });
            }
        });
    };
    // login function
    LoginComponent.prototype.userLogin = function (post) {
        var _this = this;
        // create login array
        var userObj = {
            email: post.username,
            password: post.password
        };
        this._AuthService.loginUser(userObj).subscribe(function (response) {
            if (_this._MsgHandelService.handleSuccessResponse(response)) {
                console.log(response);
                // store token
                _this._JwtService.saveToken(response.token);
                // if (
                //   this._JwtTokenValidatorService.validateUserRole('admin') ||
                //   this._JwtTokenValidatorService.validateUserRole('agent')
                // ) {
                //   // show msg
                //   this._MsgHandelService.showSuccessMsg(
                //     'Welcome to Live Trains!',
                //     'Successfully logged in!'
                //   );
                // load the dashboard
                _this._Router.navigateByUrl('/dashboard');
                // } else {
                //   this._MsgHandelService.showErrorMsg(
                //     '',
                //     'You are not authorized to login'
                //   );
                //   localStorage.removeItem('token');
                // }
            }
        }, function (error) {
            _this._MsgHandelService.handleError(error);
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/commons/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/commons/components/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_7__["MsgHandelService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_4__["JwtService"],
            _services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_8__["JwtTokenValidatorService"],
            _services_localStorageHandle_service__WEBPACK_IMPORTED_MODULE_5__["LocalStorageHandleService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/commons/components/navbar/navbar.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/commons/components/navbar/navbar.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav\r\n  #navbar\r\n  class=\"navbar navbar-expand-lg navbar-transparent  navbar-absolute navbar-container\"\r\n>\r\n  <div class=\"container-fluid\">\r\n    <div class=\"navbar-wrapper\">\r\n      <div class=\"navbar-minimize\">\r\n        <button\r\n          mat-raised-button\r\n          (click)=\"minimizeSidebar()\"\r\n          class=\"btn btn-just-icon btn-white btn-fab btn-round nav-button-wrapper\"\r\n        >\r\n          <i class=\"material-icons text_align-center visible-on-sidebar-regular\"\r\n            >more_vert</i\r\n          >\r\n          <i\r\n            class=\"material-icons design_bullet-list-67 visible-on-sidebar-mini\"\r\n            >view_list</i\r\n          >\r\n        </button>\r\n      </div>\r\n    </div>\r\n    <button\r\n      mat-button\r\n      class=\"navbar-toggler btn-no-ripple \"\r\n      type=\"button\"\r\n      (click)=\"sidebarToggle()\"\r\n    >\r\n      <span class=\"sr-only\">Toggle navigation</span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n    </button>\r\n    <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\">\r\n      <ul class=\"navbar-nav\"></ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/commons/components/navbar/navbar.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/commons/components/navbar/navbar.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dropdown-item:hover {\n  color: white !important; }\n\n.pointer-cursor {\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/commons/components/navbar/navbar.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/commons/components/navbar/navbar.component.ts ***!
  \***************************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _assets_js_route_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../assets/js/route-config */ "./src/assets/js/route-config.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _services_msg_handel_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
/* harmony import */ var _services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/jwt-token-validator.service */ "./src/app/commons/services/jwt-token-validator.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import route config



// service


var misc = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0
};
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(location, renderer, element, router, _JwtTokenValidatorService, _MsgHandelService) {
        this.renderer = renderer;
        this.element = element;
        this.router = router;
        this._JwtTokenValidatorService = _JwtTokenValidatorService;
        this._MsgHandelService = _MsgHandelService;
        this.mobile_menu_visible = 0;
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.minimizeSidebar = function () {
        var body = document.getElementsByTagName('body')[0];
        if (misc.sidebar_mini_active === true) {
            body.classList.remove('sidebar-mini');
            misc.sidebar_mini_active = false;
        }
        else {
            setTimeout(function () {
                body.classList.add('sidebar-mini');
                misc.sidebar_mini_active = true;
            }, 300);
        }
        // we simulate the window Resize so the charts will get updated in real-time.
        var simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);
        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    };
    NavbarComponent.prototype.hideSidebar = function () {
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('sidebar')[0];
        if (misc.hide_sidebar_active === true) {
            setTimeout(function () {
                body.classList.remove('hide-sidebar');
                misc.hide_sidebar_active = false;
            }, 300);
            setTimeout(function () {
                sidebar.classList.remove('animation');
            }, 600);
            sidebar.classList.add('animation');
        }
        else {
            setTimeout(function () {
                body.classList.add('hide-sidebar');
                // $('.sidebar').addClass('animation');
                misc.hide_sidebar_active = true;
            }, 300);
        }
        // we simulate the window Resize so the charts will get updated in real-time.
        var simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);
        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    };
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.listTitles = _assets_js_route_config__WEBPACK_IMPORTED_MODULE_1__["ADMIN_ROUTES"];
        // if (this._JwtTokenValidatorService.validateUserRole('admin')) {
        //   this.listTitles = ADMIN_ROUTES.filter(listTitle => listTitle);
        // } else if (this._JwtTokenValidatorService.validateUserRole('agent')) {
        //   this.listTitles = AGENT_ROUTES.filter(listTitle => listTitle);
        // }
        var navbar = this.element.nativeElement;
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        if (body.classList.contains('sidebar-mini')) {
            misc.sidebar_mini_active = true;
        }
        if (body.classList.contains('hide-sidebar')) {
            misc.hide_sidebar_active = true;
        }
        this._router = this.router.events
            .filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]; })
            .subscribe(function (event) {
            _this.sidebarClose();
            var $layer = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
            }
        });
    };
    NavbarComponent.prototype.onResize = function (event) {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    NavbarComponent.prototype.sidebarOpen = function () {
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        setTimeout(function () {
            $toggle.classList.add('toggled');
        }, 430);
        var $layer = document.createElement('div');
        $layer.setAttribute('class', 'close-layer');
        if (body.querySelectorAll('.main-panel')) {
            document.getElementsByClassName('main-panel')[0].appendChild($layer);
        }
        else if (body.classList.contains('off-canvas-sidebar')) {
            document
                .getElementsByClassName('wrapper-full-page')[0]
                .appendChild($layer);
        }
        setTimeout(function () {
            $layer.classList.add('visible');
        }, 100);
        $layer.onclick = function () {
            // assign a function
            body.classList.remove('nav-open');
            this.mobile_menu_visible = 0;
            this.sidebarVisible = false;
            $layer.classList.remove('visible');
            setTimeout(function () {
                $layer.remove();
                $toggle.classList.remove('toggled');
            }, 400);
        }.bind(this);
        body.classList.add('nav-open');
        this.mobile_menu_visible = 1;
        this.sidebarVisible = true;
    };
    NavbarComponent.prototype.sidebarClose = function () {
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        var $layer = document.createElement('div');
        $layer.setAttribute('class', 'close-layer');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
        // $('html').removeClass('nav-open');
        body.classList.remove('nav-open');
        if ($layer) {
            $layer.remove();
        }
        setTimeout(function () {
            $toggle.classList.remove('toggled');
        }, 400);
        this.mobile_menu_visible = 0;
    };
    NavbarComponent.prototype.sidebarToggle = function () {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('app-navbar-cmp'),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "button", void 0);
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar-cmp',
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/commons/components/navbar/navbar.component.scss")],
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/commons/components/navbar/navbar.component.html")
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_5__["JwtTokenValidatorService"],
            _services_msg_handel_service__WEBPACK_IMPORTED_MODULE_4__["MsgHandelService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/commons/components/navbar/navbar.module.ts":
/*!************************************************************!*\
  !*** ./src/app/commons/components/navbar/navbar.module.ts ***!
  \************************************************************/
/*! exports provided: NavbarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarModule", function() { return NavbarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navbar.component */ "./src/app/commons/components/navbar/navbar.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"]],
            declarations: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]],
            exports: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]]
        })
    ], NavbarModule);
    return NavbarModule;
}());



/***/ }),

/***/ "./src/app/commons/components/register/register.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/commons/components/register/register.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page \">\r\n  <div class=\"page-header register-page header-filter\" filter-color=\"black\">\r\n    <div class=\"container \">\r\n      <div class=\"row \">\r\n        <div class=\"col-md-10 ml-auto mr-auto\">\r\n          <div class=\"card card-signup pt-3 pb-1\">\r\n            <h3 class=\"card-title text-center \">Register</h3>\r\n            <div class=\"card-body\">\r\n              <hr />\r\n              <div class=\"row register-block\">\r\n                <div class=\"col-md-12 mr-auto\">\r\n                  <form\r\n                    class=\"form\"\r\n                    [formGroup]=\"rForm\"\r\n                    (ngSubmit)=\"registerUser(rForm.value)\"\r\n                  >\r\n                    <div class=\"mt-5 col-md-12 pl-0\">\r\n                      <div class=\"input-group\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">\r\n                            <i class=\"material-icons\">mail</i>\r\n                          </span>\r\n                        </div>\r\n                        <input\r\n                          type=\"email\"\r\n                          class=\"form-control\"\r\n                          placeholder=\"Email\"\r\n                          formControlName=\"email\"\r\n                        />\r\n                      </div>\r\n                      <div\r\n                        class=\"error-msg\"\r\n                        *ngIf=\"\r\n                          !rForm.controls['email'].valid &&\r\n                          rForm.controls['email'].touched\r\n                        \"\r\n                      >\r\n                        <small>Valid Email <strong>required</strong> </small>\r\n                      </div>\r\n                    </div>\r\n\r\n                    \r\n\r\n                    \r\n\r\n                    \r\n\r\n                    <div class=\"col-md-12 row pt-4 pr-0\">\r\n                      <div class=\"col-md-6 px-0\">\r\n                        <div class=\"input-group\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text\">\r\n                              <i class=\"material-icons\">lock_outline</i>\r\n                            </span>\r\n                          </div>\r\n                          <input\r\n                            [type]=\"hide ? 'text' : 'password'\"\r\n                            placeholder=\"Password\"\r\n                            class=\"form-control\"\r\n                            formControlName=\"password\"\r\n                          />\r\n                          <mat-icon\r\n                            class=\"password-show mt-2\"\r\n                            matSuffix\r\n                            (click)=\"hide = !hide\"\r\n                            >{{\r\n                              hide ? 'visibility' : 'visibility_off'\r\n                            }}</mat-icon\r\n                          >\r\n                        </div>\r\n                        <div\r\n                          class=\"error-msg\"\r\n                          *ngIf=\"\r\n                            !rForm.controls['password'].valid &&\r\n                            rForm.controls['password'].touched\r\n                          \"\r\n                        >\r\n                          <small\r\n                            >Password <strong>required</strong> and need to have\r\n                            at least <strong>8</strong> characters.</small\r\n                          >\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"col-md-6 px-0\">\r\n                        <div class=\"input-group\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text\">\r\n                              <i class=\"material-icons\">lock</i>\r\n                            </span>\r\n                          </div>\r\n                          <input\r\n                            [type]=\"hide2 ? 'text' : 'password'\"\r\n                            class=\"form-control\"\r\n                            placeholder=\"Confirm Password\"\r\n                            formControlName=\"confirmPassword\"\r\n                          />\r\n                          <mat-icon\r\n                            class=\"password-show mt-2\"\r\n                            matSuffix\r\n                            (click)=\"hide2 = !hide2\"\r\n                            >{{\r\n                              hide2 ? 'visibility' : 'visibility_off'\r\n                            }}</mat-icon\r\n                          >\r\n                        </div>\r\n                        <div\r\n                          class=\"error-msg\"\r\n                          *ngIf=\"\r\n                            !rForm.controls['confirmPassword'].valid &&\r\n                            rForm.controls['confirmPassword'].touched\r\n                          \"\r\n                        >\r\n                          <small\r\n                            >Confirm Password <strong>not match </strong> with\r\n                            the password\r\n                          </small>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div\r\n                      class=\"col-md-12 px-0 d-flex justify-content-center align-items-center pt-3\"\r\n                    >\r\n                      <div class=\"form-check mb-1 px-0\">\r\n                        <label class=\"form-check-label pr-0\">\r\n                          <input\r\n                            class=\"form-check-input\"\r\n                            type=\"checkbox\"\r\n                            value=\"\"\r\n                            checked=\"\"\r\n                            formControlName=\"agreement\"\r\n                          />\r\n                          <span class=\"form-check-sign\">\r\n                            <span class=\"check\"></span>\r\n                          </span>\r\n                          I agree to the\r\n                          <a href=\"#something\">terms and conditions</a>.\r\n                        </label>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"text-center\">\r\n                      <button\r\n                        class=\"btn btn-primary btn-round mt-4 mr-3\"\r\n                        [routerLink]=\"['/login']\"\r\n                      >\r\n                        Back\r\n                      </button>\r\n                      <button\r\n                        class=\"btn btn-primary btn-round mt-4\"\r\n                        [disabled]=\"!rForm.valid\"\r\n                        type=\"submit\"\r\n                      >\r\n                        Get Started\r\n                      </button>\r\n                    </div>\r\n                  </form>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <app-footer-cmp class=\"footer-display\"></app-footer-cmp>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/commons/components/register/register.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/commons/components/register/register.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error-msg {\n  margin-top: -5px;\n  margin-left: 50px;\n  color: red; }\n\n.footer-popup {\n  z-index: 1000; }\n\n.register-page {\n  margin: 0 !important; }\n\n.card-signup {\n  margin: 0 !important; }\n\n.img-tag {\n  height: 100px; }\n\n.password-show {\n  cursor: pointer; }\n\n.wrapper-full-page {\n  background: url('background.jpg');\n  background-repeat: no-repeat;\n  background-size: cover; }\n\n.register-block {\n  margin-top: -32px !important; }\n\n.footer-display {\n  display: block; }\n\n@media only screen and (max-width: 600px) {\n  .footer-display {\n    display: none; } }\n"

/***/ }),

/***/ "./src/app/commons/components/register/register.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/commons/components/register/register.component.ts ***!
  \*******************************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/commons/services/authentication.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _assets_js_user_configs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../assets/js/user-configs */ "./src/assets/js/user-configs.ts");
/* harmony import */ var _validations_validator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../validations/validator */ "./src/app/commons/validations/validator.ts");
/* harmony import */ var _commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../commons/services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/* import service */

// import form modules


// import common libraries



var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(_AuthService, _FormBuilder, _Router, _MsgHandelService) {
        this._AuthService = _AuthService;
        this._FormBuilder = _FormBuilder;
        this._Router = _Router;
        this._MsgHandelService = _MsgHandelService;
        this.imageUploadURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].api_url + "uploads";
        this.serverImageUploadPath = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].imageUploadPath;
        this.maxPhotos = 1;
        this.imageTypes = ['jpeg', 'png'];
        this.uploadedPhoto = null;
        // email regex
        this.emailPattern = /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{1,63}$/;
        // declare constants
        this.USER_ROLE = _assets_js_user_configs__WEBPACK_IMPORTED_MODULE_4__["USER_ROLE"].AGENT;
        this.hide = false;
        this.hide2 = false;
        this.test = new Date();
        this.rForm = this._FormBuilder.group({
            email: [
                null,
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
                ]
            ],
            password: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8)])
            ],
            confirmPassword: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _validations_validator__WEBPACK_IMPORTED_MODULE_5__["confPassValidation"]]]
        });
    }
    RegisterComponent.prototype.ngOnInit = function () {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('register-page');
        body.classList.add('off-canvas-sidebar');
    };
    RegisterComponent.prototype.ngOnDestroy = function () {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('register-page');
        body.classList.remove('off-canvas-sidebar');
    };
    /**
     * Register user
     * @param post : FromGroupValues
     */
    RegisterComponent.prototype.registerUser = function (post) {
        var _this = this;
        // create update array
        var userObj = {
            email: post.email,
            password: post.password,
        };
        this._AuthService.registerUser(userObj).subscribe(function (data) {
            if (_this._MsgHandelService.handleSuccessResponse(data)) {
                // reload the data
                _this._Router.navigate(['/login']);
                // show msg
                _this._MsgHandelService.showSuccessMsg('successful!', 'You have successfully registered with the Live Trains Admin!');
            }
        }, function (error) {
            // show msg
            _this._MsgHandelService.handleError(error);
        });
    };
    // after upload the photo this function will fire
    RegisterComponent.prototype.onUploadFinished = function (event) {
        this.uploadedPhoto =
            event.serverResponse.response.body['data']['value']['_id'];
        console.log(this.uploadedPhoto);
    };
    RegisterComponent.prototype.onRemoved = function (event) {
        // if the image is removed from the selection
        this.uploadedPhoto = null;
        console.log(this.uploadedPhoto);
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/commons/components/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/commons/components/register/register.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_6__["MsgHandelService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/commons/components/sidebar/sidebar.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/commons/components/sidebar/sidebar.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"logo \">\r\n  <a class=\"simple-text\">\r\n    <div class=\"text-center\">\r\n      <img\r\n        class=\"img-logo\"\r\n        src=\"assets/img/live train logo.png\"\r\n      />\r\n    </div>\r\n  </a>\r\n  <div class=\"simple-text logo-normal text-capitalize \"></div>\r\n</div>\r\n\r\n<div class=\"sidebar-wrapper\">\r\n  <div class=\"user\" *ngIf=\"!is_admin\">\r\n    <div class=\"photo\"><img src=\"{{ photo }}\" /></div>\r\n    <div class=\"user-info\">\r\n      <a data-toggle=\"collapse\" href=\"#collapseExample\" class=\"collapsed\">\r\n        <span> {{ username }} <b class=\"caret\"></b> </span>\r\n      </a>\r\n      <div class=\"collapse\" id=\"collapseExample\">\r\n        <ul class=\"nav\">\r\n          <li class=\"nav-item\">\r\n            <a\r\n              class=\"nav-link\"\r\n              [routerLink]=\"['/user-profile']\"\r\n            >\r\n              <span class=\"sidebar-mini\">P</span>\r\n              <span class=\"sidebar-normal\">Profile</span>\r\n            </a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <ul class=\"nav\">\r\n    <li\r\n      routerLinkActive=\"active\"\r\n      *ngFor=\"let menuitem of menuItems\"\r\n      class=\"nav-item\"\r\n    >\r\n      <!-- If is a single link -->\r\n      <a\r\n        [routerLink]=\"[menuitem.path]\"\r\n        *ngIf=\"menuitem.type === 'link'\"\r\n        class=\"nav-link\"\r\n      >\r\n        <i class=\"material-icons\">{{ menuitem.icontype }}</i>\r\n        <p>{{ menuitem.title }}</p>\r\n      </a>\r\n      <!-- If it have a sub-menu -->\r\n      <a\r\n        data-toggle=\"collapse\"\r\n        href=\"#{{ menuitem.collapse }}\"\r\n        *ngIf=\"menuitem.type === 'sub'\"\r\n        (click)=\"updatePS()\"\r\n        class=\"nav-link\"\r\n        style=\"color: #efae00 \"\r\n      >\r\n        <i class=\"material-icons\" style=\"color: #efae00 \">{{\r\n          menuitem.icontype\r\n        }}</i>\r\n        <p>{{ menuitem.title }}<b class=\"caret\"></b></p>\r\n      </a>\r\n\r\n      <!-- Display the sub-menu items -->\r\n      <div\r\n        id=\"{{ menuitem.collapse }}\"\r\n        class=\"collapse\"\r\n        *ngIf=\"menuitem.type === 'sub'\"\r\n      >\r\n        <ul class=\"nav\">\r\n          <li\r\n            routerLinkActive=\"active\"\r\n            *ngFor=\"let child_item of menuitem.children\"\r\n            class=\"nav-item\"\r\n          >\r\n            <a [routerLink]=\"[menuitem.path, child_item.path]\" class=\"nav-link\">\r\n              <span class=\"sidebar-mini\">{{ child_item.ab }}</span>\r\n              <span class=\"sidebar-normal\">{{ child_item.title }}</span>\r\n            </a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </li>\r\n    <li>\r\n      <a (click)=\"logOut()\" class=\"nav-link\" style=\"cursor:pointer\">\r\n        <i class=\"material-icons\">power_settings_new</i>\r\n        <p style=\"color: white\">LogOut</p>\r\n      </a>\r\n    </li>\r\n  </ul>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/commons/components/sidebar/sidebar.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/commons/components/sidebar/sidebar.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/commons/components/sidebar/sidebar.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/commons/components/sidebar/sidebar.component.ts ***!
  \*****************************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! perfect-scrollbar */ "./node_modules/perfect-scrollbar/dist/perfect-scrollbar.esm.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/commons/services/authentication.service.ts");
/* harmony import */ var _services_msg_handel_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
/* harmony import */ var _services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/jwt-token-validator.service */ "./src/app/commons/services/jwt-token-validator.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _assets_js_route_config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../assets/js/route-config */ "./src/assets/js/route-config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import service




// import route config

var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(_Router, _AuthService, _MsgHandelService, _JwtTokenValidatorService) {
        this._Router = _Router;
        this._AuthService = _AuthService;
        this._MsgHandelService = _MsgHandelService;
        this._JwtTokenValidatorService = _JwtTokenValidatorService;
        this.photo = 'assets/img/default-avatar.png';
        this.is_admin = false;
    }
    SidebarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    SidebarComponent.prototype.ngOnInit = function () {
        this.is_admin = true;
        this.menuItems = _assets_js_route_config__WEBPACK_IMPORTED_MODULE_7__["ADMIN_ROUTES"];
        // if (this._JwtTokenValidatorService.validateUserRole('admin')) {
        //   this.is_admin = true;
        //   this.menuItems = ADMIN_ROUTES.filter(menuItem => menuItem);
        // } else if (this._JwtTokenValidatorService.validateUserRole('agent')) {
        //   this.showImage();
        //   this.is_admin = false;
        //   this.menuItems = AGENT_ROUTES.filter(menuItem => menuItem);
        // }
        // set user email as the user name
        // this.username = this._LocalStorageHandleService.getItem('email');
    };
    // showImage() {
    //   this._AuthService
    //     .getSingleUser(this._JwtTokenValidatorService.accessUser_Id())
    //     .subscribe(
    //       response => {
    //         if (this._MsgHandelService.handleSuccessResponse(response)) {
    //           const apiData = response['data']['value'];
    //           if (
    //             apiData.photo !== undefined &&
    //             apiData.photo !== null &&
    //             !this.is_admin
    //           ) {
    //             const image_name = apiData['photo']['file_name'];
    //             this.photo = `${
    //               environment.imageUploadPath
    //               }/300x300/${image_name}`;
    //           }
    //         }
    //       },
    //       error => {
    //         this._MsgHandelService.handleError(error);
    //       }
    //     );
    // }
    SidebarComponent.prototype.updatePS = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemSidebar = (document.querySelector('.sidebar .sidebar-wrapper'));
            var ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__["default"](elemSidebar, {
                wheelSpeed: 2,
                suppressScrollX: true
            });
        }
    };
    SidebarComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 ||
            navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    SidebarComponent.prototype.navigateToProfile = function () {
        this._Router.navigate(['/user-profile'], {
            queryParams: {
                operation: 'edit_profile',
                userId: this._JwtTokenValidatorService.getLoggedUserId()
            }
        });
    };
    SidebarComponent.prototype.logOut = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default()({
            title: 'Are you sure?',
            text: 'Are you sure that you want to Log out?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function (willDelete) {
            if (willDelete.value) {
                window.localStorage.clear();
                _this._MsgHandelService.showSuccessMsg('Signed out!', 'successfully logout');
                _this._Router.navigateByUrl('/login');
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        });
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar-cmp',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/commons/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/commons/components/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _services_msg_handel_service__WEBPACK_IMPORTED_MODULE_4__["MsgHandelService"],
            _services_jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_5__["JwtTokenValidatorService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/commons/components/sidebar/sidebar.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/commons/components/sidebar/sidebar.module.ts ***!
  \**************************************************************/
/*! exports provided: SidebarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarModule", function() { return SidebarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebar.component */ "./src/app/commons/components/sidebar/sidebar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SidebarModule = /** @class */ (function () {
    function SidebarModule() {
    }
    SidebarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]],
            exports: [_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]]
        })
    ], SidebarModule);
    return SidebarModule;
}());



/***/ }),

/***/ "./src/app/commons/layouts/admin/admin-layout.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/commons/layouts/admin/admin-layout.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\">\r\n  <div\r\n    class=\"sidebar\"\r\n    data-color=\"white\"\r\n    data-background-color=\"black\"\r\n    data-image=\"./assets/img/sidebar-1.jpg\"\r\n  >\r\n    <app-sidebar-cmp></app-sidebar-cmp>\r\n  </div>\r\n  <div class=\"main-panel\">\r\n    <app-navbar-cmp></app-navbar-cmp>\r\n    <router-outlet></router-outlet>\r\n    <app-footer-cmp class=\"footer-display\"></app-footer-cmp>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/commons/layouts/admin/admin-layout.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/commons/layouts/admin/admin-layout.component.ts ***!
  \*****************************************************************/
/*! exports provided: AdminLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutComponent", function() { return AdminLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var _commons_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../commons/components/navbar/navbar.component */ "./src/app/commons/components/navbar/navbar.component.ts");
/* harmony import */ var perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! perfect-scrollbar */ "./node_modules/perfect-scrollbar/dist/perfect-scrollbar.esm.js");
/* harmony import */ var _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../modules/dashboard/md/md.module */ "./src/app/modules/dashboard/md/md.module.ts");
/* harmony import */ var _assets_js_particleJs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../assets/js/particleJs */ "./src/assets/js/particleJs.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import modules

// import particle js

var AdminLayoutComponent = /** @class */ (function () {
    function AdminLayoutComponent(router, location) {
        this.router = router;
        // particle js
        this.myStyle = {};
        this.myParams = {};
        this.width = 100;
        this.height = 99;
        this.yScrollStack = [];
        this.location = location;
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var elemMainPanel = document.querySelector('.main-panel');
        var elemSidebar = (document.querySelector('.sidebar .sidebar-wrapper'));
        this.location.subscribe(function (ev) {
            _this.lastPoppedUrl = ev.url;
        });
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                if (event.url != _this.lastPoppedUrl)
                    _this.yScrollStack.push(window.scrollY);
            }
            else if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                if (event.url == _this.lastPoppedUrl) {
                    _this.lastPoppedUrl = undefined;
                    window.scrollTo(0, _this.yScrollStack.pop());
                }
                else
                    window.scrollTo(0, 0);
            }
        });
        this._router = this.router.events
            .filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; })
            .subscribe(function (event) {
            elemMainPanel.scrollTop = 0;
            elemSidebar.scrollTop = 0;
        });
        var html = document.getElementsByTagName('html')[0];
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["default"](elemMainPanel);
            ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["default"](elemSidebar);
            html.classList.add('perfect-scrollbar-on');
        }
        else {
            html.classList.add('perfect-scrollbar-off');
        }
        this._router = this.router.events
            .filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; })
            .subscribe(function (event) {
            _this.navbar.sidebarClose();
        });
        this.navItems = [
            {
                type: _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__["NavItemType"].NavbarLeft,
                title: 'Dashboard',
                iconClass: 'fa fa-dashboard'
            },
            {
                type: _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__["NavItemType"].NavbarRight,
                title: '',
                iconClass: 'fa fa-bell-o',
                numNotifications: 5,
                dropdownItems: [
                    { title: 'Notification 1' },
                    { title: 'Notification 2' },
                    { title: 'Notification 3' },
                    { title: 'Notification 4' },
                    { title: 'Another Notification' }
                ]
            },
            {
                type: _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__["NavItemType"].NavbarRight,
                title: '',
                iconClass: 'fa fa-list',
                dropdownItems: [
                    { iconClass: 'pe-7s-mail', title: 'Messages' },
                    { iconClass: 'pe-7s-help1', title: 'Help Center' },
                    { iconClass: 'pe-7s-tools', title: 'Settings' },
                    'separator',
                    { iconClass: 'pe-7s-lock', title: 'Lock Screen' },
                    { iconClass: 'pe-7s-close-circle', title: 'Log Out' }
                ]
            },
            {
                type: _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__["NavItemType"].NavbarLeft,
                title: 'Search',
                iconClass: 'fa fa-search'
            },
            { type: _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__["NavItemType"].NavbarLeft, title: 'Account' },
            {
                type: _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__["NavItemType"].NavbarLeft,
                title: 'Dropdown',
                dropdownItems: [
                    { title: 'Action' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    'separator',
                    { title: 'Separated link' }
                ]
            },
            { type: _modules_dashboard_md_md_module__WEBPACK_IMPORTED_MODULE_6__["NavItemType"].NavbarLeft, title: 'Log out' }
        ];
        // init particle js
        this.myStyle = _assets_js_particleJs__WEBPACK_IMPORTED_MODULE_7__["particleStyle"];
        this.myParams = _assets_js_particleJs__WEBPACK_IMPORTED_MODULE_7__["particleProperties"];
    };
    AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        this.runOnRouteChange();
    };
    AdminLayoutComponent.prototype.isMap = function () {
        if (this.location.prepareExternalUrl(this.location.path()) ===
            '/maps/fullscreen') {
            return true;
        }
        else {
            return false;
        }
    };
    AdminLayoutComponent.prototype.runOnRouteChange = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemSidebar = (document.querySelector('.sidebar .sidebar-wrapper'));
            var elemMainPanel = document.querySelector('.main-panel');
            var ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["default"](elemMainPanel);
            ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["default"](elemSidebar);
            ps.update();
        }
    };
    AdminLayoutComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 ||
            navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sidebar'),
        __metadata("design:type", Object)
    ], AdminLayoutComponent.prototype, "sidebar", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_commons_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__["NavbarComponent"]),
        __metadata("design:type", _commons_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__["NavbarComponent"])
    ], AdminLayoutComponent.prototype, "navbar", void 0);
    AdminLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./admin-layout.component.html */ "./src/app/commons/layouts/admin/admin-layout.component.html"),
            styles: [__webpack_require__(/*! ./admin-layout.scss */ "./src/app/commons/layouts/admin/admin-layout.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());



/***/ }),

/***/ "./src/app/commons/layouts/admin/admin-layout.scss":
/*!*********************************************************!*\
  !*** ./src/app/commons/layouts/admin/admin-layout.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".footer-display {\n  display: block; }\n\n@media only screen and (max-width: 600px) {\n  .footer-display {\n    display: none; } }\n"

/***/ }),

/***/ "./src/app/commons/layouts/auth/auth-layout.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/commons/layouts/auth/auth-layout.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg bg-primary navbar-transparent navbar-absolute\" color-on-scroll=\"500\">\r\n  <div class=\"container\">\r\n    <div class=\"navbar-wrapper\">\r\n      <a class=\"navbar-brand d-none d-sm-none d-md-block\" [routerLink]=\"['/dashboard']\">Material Dashboard Pro Angular</a>\r\n      <a class=\"navbar-brand d-block d-sm-block d-md-none\" [routerLink]=\"['/dashboard']\">MD Pro Angular</a>\r\n    </div>\r\n    <button mat-button class=\"navbar-toggler\" type=\"button\" (click)=\"sidebarToggle()\">\r\n      <span class=\"sr-only\">Toggle navigation</span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n    </button>\r\n    <div class=\"collapse navbar-collapse justify-content-end\">\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/dashboard']\">\r\n            <i class=\"material-icons\">dashboard</i> Dashboard\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/pages/register']\">\r\n            <i class=\"material-icons\">person_add</i> Register\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/pages/login']\">\r\n            <i class=\"material-icons\">fingerprint</i> Login\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/pages/lock']\">\r\n            <i class=\"material-icons\">lock_open</i> Lock\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n  <router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/commons/layouts/auth/auth-layout.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/commons/layouts/auth/auth-layout.component.ts ***!
  \***************************************************************/
/*! exports provided: AuthLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthLayoutComponent", function() { return AuthLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthLayoutComponent = /** @class */ (function () {
    function AuthLayoutComponent(router, element) {
        this.router = router;
        this.element = element;
        this.mobile_menu_visible = 0;
        this.sidebarVisible = false;
    }
    AuthLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            _this.sidebarClose();
        });
    };
    AuthLayoutComponent.prototype.sidebarOpen = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    ;
    AuthLayoutComponent.prototype.sidebarClose = function () {
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    ;
    AuthLayoutComponent.prototype.sidebarToggle = function () {
        var body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
            var $layer = document.createElement('div');
            $layer.setAttribute('class', 'close-layer');
            if (body.querySelectorAll('.wrapper-full-page')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }
            else if (body.classList.contains('off-canvas-sidebar')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }
            setTimeout(function () {
                $layer.classList.add('visible');
            }, 100);
            $layer.onclick = function () {
                body.classList.remove('nav-open');
                this.mobile_menu_visible = 0;
                $layer.classList.remove('visible');
                this.sidebarClose();
            }.bind(this);
            body.classList.add('nav-open');
        }
        else {
            document.getElementsByClassName("close-layer")[0].remove();
            this.sidebarClose();
        }
    };
    AuthLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./auth-layout.component.html */ "./src/app/commons/layouts/auth/auth-layout.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());



/***/ }),

/***/ "./src/app/commons/pipes/date-difference.pipe.ts":
/*!*******************************************************!*\
  !*** ./src/app/commons/pipes/date-difference.pipe.ts ***!
  \*******************************************************/
/*! exports provided: DateDifferencePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateDifferencePipe", function() { return DateDifferencePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DateDifferencePipe = /** @class */ (function () {
    function DateDifferencePipe() {
    }
    DateDifferencePipe.prototype.transform = function (value, args) {
        var diff = Math.floor(new Date(args).getTime() - new Date(value).getTime());
        var day = 1000 * 60 * 60 * 24;
        var days = Math.floor(diff / day);
        var months = Math.floor(days / 31);
        var years = Math.floor(months / 12);
        // final values
        var fYear = years;
        var fMonth = months - fYear * 12;
        return fYear === 0 && fMonth === 0
            ? days + " Days"
            : fYear + " " + (fYear > 1 ? 'years ' : 'year ') + " - " + fMonth + " " + (fMonth > 1 ? 'months ' : 'month ');
    };
    DateDifferencePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'dateDifference'
        })
    ], DateDifferencePipe);
    return DateDifferencePipe;
}());



/***/ }),

/***/ "./src/app/commons/pipes/order-by.pipe.ts":
/*!************************************************!*\
  !*** ./src/app/commons/pipes/order-by.pipe.ts ***!
  \************************************************/
/*! exports provided: OrderByPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderByPipe", function() { return OrderByPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var OrderByPipe = /** @class */ (function () {
    function OrderByPipe() {
    }
    OrderByPipe.prototype.transform = function (input, key) {
        if (!input) {
            return [];
        }
        return input.sort(function (itemA, itemB) {
            if (itemA[key] > itemB[key]) {
                return 1;
            }
            else if (itemA[key] < itemB[key]) {
                return -1;
            }
            else {
                return 0;
            }
        });
    };
    OrderByPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'orderBy'
        })
    ], OrderByPipe);
    return OrderByPipe;
}());



/***/ }),

/***/ "./src/app/commons/pipes/year-month.pipe.ts":
/*!**************************************************!*\
  !*** ./src/app/commons/pipes/year-month.pipe.ts ***!
  \**************************************************/
/*! exports provided: YearMonthPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YearMonthPipe", function() { return YearMonthPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var YearMonthPipe = /** @class */ (function () {
    function YearMonthPipe() {
    }
    YearMonthPipe.prototype.transform = function (value, args) {
        if (value !== undefined && value !== null) {
            return new Date(value).getFullYear() + "-" + ('0' +
                (new Date(value).getMonth() + 1)).slice(-2);
        }
        else {
            return null;
        }
    };
    YearMonthPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'yearMonth'
        })
    ], YearMonthPipe);
    return YearMonthPipe;
}());



/***/ }),

/***/ "./src/app/commons/services/authentication.service.ts":
/*!************************************************************!*\
  !*** ./src/app/commons/services/authentication.service.ts ***!
  \************************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _infrastructure_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../infrastructure/api.service */ "./src/app/infrastructure/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * import services
 */

var AuthService = /** @class */ (function () {
    function AuthService(_MainService) {
        this._MainService = _MainService;
        // user photo broadcaster
        this.photo = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('');
        this.photoCast = this.photo.asObservable();
        // user name broadcaster
        this.name = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('');
        this.nameCast = this.name.asObservable();
    }
    AuthService.prototype.updatePhoto = function (newPhoto) {
        this.photo.next(newPhoto);
    };
    AuthService.prototype.updateName = function (newName) {
        this.name.next(newName);
    };
    /**
     * calling to login endpoint
     * @param user : userObject<email,password>
     */
    AuthService.prototype.loginUser = function (user) {
        return this._MainService.post('user/login', user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (res) {
            throw res;
        }));
    };
    /**
     * calling to forget password
     * @param user : userObject<email>
     */
    AuthService.prototype.forgerPassword = function (user) {
        return this._MainService.post('password/forget', user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (res) {
            throw res;
        }));
    };
    /**
     * Register User
     * @param user : userObject
     */
    AuthService.prototype.registerUser = function (user) {
        return this._MainService.post('user/new', user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (res) {
            throw res;
        }));
    };
    /**
     * Get Single User
     * @param user : userObject
     */
    AuthService.prototype.getSingleUser = function (id) {
        return this._MainService.get("users/getOne/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (res) {
            throw res;
        }));
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_infrastructure_api_service__WEBPACK_IMPORTED_MODULE_3__["MainService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/commons/services/authorization-route-guard.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/commons/services/authorization-route-guard.service.ts ***!
  \***********************************************************************/
/*! exports provided: AuthRouteGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRouteGuardService", function() { return AuthRouteGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./jwt-token-validator.service */ "./src/app/commons/services/jwt-token-validator.service.ts");
/* harmony import */ var _commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../commons/services/msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthRouteGuardService = /** @class */ (function () {
    function AuthRouteGuardService(_Router, _MsgHandelService, _JwtTokenValidatorService) {
        this._Router = _Router;
        this._MsgHandelService = _MsgHandelService;
        this._JwtTokenValidatorService = _JwtTokenValidatorService;
    }
    AuthRouteGuardService.prototype.canActivate = function (route, state) {
        // get all routes
        var routes = this._JwtTokenValidatorService.getAuthorizedRoutes();
        var url = state.url;
        // filter URL and remove query parameters
        if (state.url.includes('?')) {
            url = state.url.split('?')[0];
        }
        if (routes !== null) {
            if (routes.includes(url)) {
                return true;
            }
        }
        this._MsgHandelService.showErrorMsg('Unauthorized', 'You are not allow to access this route');
        return false;
    };
    AuthRouteGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _commons_services_msg_handel_service__WEBPACK_IMPORTED_MODULE_3__["MsgHandelService"],
            _jwt_token_validator_service__WEBPACK_IMPORTED_MODULE_2__["JwtTokenValidatorService"]])
    ], AuthRouteGuardService);
    return AuthRouteGuardService;
}());



/***/ }),

/***/ "./src/app/commons/services/jwt-token-validator.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/commons/services/jwt-token-validator.service.ts ***!
  \*****************************************************************/
/*! exports provided: JwtTokenValidatorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtTokenValidatorService", function() { return JwtTokenValidatorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _localStorageHandle_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./localStorageHandle.service */ "./src/app/commons/services/localStorageHandle.service.ts");
/* harmony import */ var _msg_handel_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./msg-handel.service */ "./src/app/commons/services/msg-handel.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import jwt token decoder

// import local storage handle service

// message handle service

var JwtTokenValidatorService = /** @class */ (function () {
    function JwtTokenValidatorService(_LocalStorageHandleService, _MsgHandelService) {
        this._LocalStorageHandleService = _LocalStorageHandleService;
        this._MsgHandelService = _MsgHandelService;
    }
    JwtTokenValidatorService.prototype.validateUserRole = function (userRole) {
        try {
            var tokenBody = jwt_decode__WEBPACK_IMPORTED_MODULE_1__(this._LocalStorageHandleService.getItem('token'));
            // console.log(tokenBody.role);
            if (userRole === tokenBody.role) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Error) {
            this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
            return false;
        }
    };
    JwtTokenValidatorService.prototype.getLoggedUserId = function () {
        try {
            var tokenBody = jwt_decode__WEBPACK_IMPORTED_MODULE_1__(this._LocalStorageHandleService.getItem('token'));
            return tokenBody['_id'] === undefined ? null : tokenBody['_id'];
        }
        catch (Error) {
            this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
            return null;
        }
    };
    JwtTokenValidatorService.prototype.accessUser_Id = function () {
        try {
            var tokenBody = jwt_decode__WEBPACK_IMPORTED_MODULE_1__(this._LocalStorageHandleService.getItem('token'));
            return tokenBody['_id'] === undefined ? null : tokenBody['_id'];
        }
        catch (Error) {
            this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
            return null;
        }
    };
    JwtTokenValidatorService.prototype.getLoggedUserName = function () {
        try {
            var tokenBody = jwt_decode__WEBPACK_IMPORTED_MODULE_1__(this._LocalStorageHandleService.getItem('token'));
            return tokenBody['user_name'] === undefined
                ? null
                : tokenBody['user_name'];
        }
        catch (Error) {
            this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
            return null;
        }
    };
    JwtTokenValidatorService.prototype.getAuthorizedRoutes = function () {
        try {
            var tokenBody = jwt_decode__WEBPACK_IMPORTED_MODULE_1__(this._LocalStorageHandleService.getItem('token'));
            return tokenBody['routs'] === undefined ? null : tokenBody['routs'];
        }
        catch (Error) {
            this._MsgHandelService.showErrorMsg('Error', 'Could not read token');
            return null;
        }
    };
    JwtTokenValidatorService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_localStorageHandle_service__WEBPACK_IMPORTED_MODULE_2__["LocalStorageHandleService"],
            _msg_handel_service__WEBPACK_IMPORTED_MODULE_3__["MsgHandelService"]])
    ], JwtTokenValidatorService);
    return JwtTokenValidatorService;
}());



/***/ }),

/***/ "./src/app/commons/services/localStorageHandle.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/commons/services/localStorageHandle.service.ts ***!
  \****************************************************************/
/*! exports provided: LocalStorageHandleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStorageHandleService", function() { return LocalStorageHandleService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LocalStorageHandleService = /** @class */ (function () {
    function LocalStorageHandleService() {
    }
    /**
     * get any item from local storage using property name
     * @param name : String
     */
    LocalStorageHandleService.prototype.getItem = function (name) {
        return window.localStorage[name];
    };
    /**
     * save any item in local storage
     * @param item : Object (name,value)
     */
    LocalStorageHandleService.prototype.saveItem = function (item) {
        window.localStorage[item.name] = item.value;
    };
    /**
     * save array of items in local storage
     * @param items : Array <Object(name,value)>
     */
    LocalStorageHandleService.prototype.saveMultipleItems = function (items) {
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            window.localStorage[item.name] = item.value;
        }
    };
    /**
     * remove item from local storage by parsing the property name
     * @param name : String
     */
    LocalStorageHandleService.prototype.destroyItem = function (name) {
        window.localStorage.removeItem(name);
    };
    /**
     * remove all data from loacal storage
     */
    LocalStorageHandleService.prototype.destroyAll = function () {
        window.localStorage.clear();
    };
    LocalStorageHandleService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LocalStorageHandleService);
    return LocalStorageHandleService;
}());



/***/ }),

/***/ "./src/app/commons/services/msg-handel.service.ts":
/*!********************************************************!*\
  !*** ./src/app/commons/services/msg-handel.service.ts ***!
  \********************************************************/
/*! exports provided: MsgHandelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MsgHandelService", function() { return MsgHandelService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../infrastructure/jwt.service */ "./src/app/infrastructure/jwt.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * services import
 */

/**
 * common services
 */


var MsgHandelService = /** @class */ (function () {
    function MsgHandelService(_Router, _JwtService, _ToastrService) {
        this._Router = _Router;
        this._JwtService = _JwtService;
        this._ToastrService = _ToastrService;
        // declare error msg
        this.REFRESH_MESSAGE = 'Something went wrong. Please Login again!';
        this.DEFAULT_ERROR_TITLE = 'Something went wrong';
    }
    /**
     *
     * @param response : response getting from the api call
     */
    MsgHandelService.prototype.handleSuccessResponse = function (response) {
        console.log(response['error']);
        console.log(response['data']);
        if (response['status'] === true) {
            if (response['data'] === undefined) {
                this._ToastrService.error('UnSupported Data Format From Server!', 'Error');
                return false;
            }
            else {
                if (response['data'] !== undefined) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        else {
            this._ToastrService.error(response['error'].message, 'Error');
            return false;
        }
    };
    /**
     * show success msg as notifications
     * @param title : string
     * @param content : string
     */
    MsgHandelService.prototype.showSuccessMsg = function (title, content) {
        this._ToastrService.success(content, title);
    };
    /**
     * show success msg as notifications
     * @param title : string
     * @param content : string
     */
    MsgHandelService.prototype.showWarningMsg = function (title, content) {
        this._ToastrService.warning(content, title);
    };
    /**
     * show Error msg as notifications
     * @param title : string
     * @param content : string
     */
    MsgHandelService.prototype.showErrorMsg = function (title, content) {
        this._ToastrService.error(content, title);
    };
    /**
     * handle common errors
     * @param error : error with the response
     */
    MsgHandelService.prototype.handleError = function (error) {
        var errorMsg = '';
        var errorTitle = 'Connection Issue';
        var httpErrorCode = error.httpErrorCode;
        console.log(error);
        if (error.msg.includes("already")) {
            errorTitle = error.msg;
            this.showError(errorTitle, errorTitle);
        }
        else {
            if (error['error'] !== undefined && error['error'] !== null) {
                if (error['error']['statusCode'] === 401) {
                    errorMsg = 'You Unauthorized to proceed this action !';
                }
                else if (error['error']['statusCode'] === 400) {
                    errorMsg = 'Request data incomplete !';
                }
                else {
                    errorMsg = error['error']['message'];
                }
                errorTitle = error['error']['name'];
            }
            this.showError(errorTitle, errorMsg);
        }
        // call for customized error handle
    };
    /**
     * show error msg in common way
     * @param message : error msg
     */
    MsgHandelService.prototype.showError = function (title, message) {
        this._ToastrService.error(message, title);
    };
    MsgHandelService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _infrastructure_jwt_service__WEBPACK_IMPORTED_MODULE_1__["JwtService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], MsgHandelService);
    return MsgHandelService;
}());



/***/ }),

/***/ "./src/app/commons/validations/validator.ts":
/*!**************************************************!*\
  !*** ./src/app/commons/validations/validator.ts ***!
  \**************************************************/
/*! exports provided: regexValidators, confPassValidation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "regexValidators", function() { return regexValidators; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confPassValidation", function() { return confPassValidation; });
var pureEmail = '/^(([^<>()[]\\.,;:s@"]+(.[^<>()[]\\.,;:s@"]+)*)|(".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/';
var regexValidators = {
    phone: '[+][0-9() ]{7,}$',
    email: pureEmail
};
function confPassValidation(control) {
    if (control && (control.value !== null && control.value !== undefined)) {
        var confPassword = control.value;
        var passwordControl = control.root.get('password');
        if (passwordControl) {
            var password = passwordControl.value;
            if (password !== confPassword) {
                return {
                    isError: true
                };
            }
        }
    }
    return null;
}


/***/ }),

/***/ "./src/app/filter.pipe.ts":
/*!********************************!*\
  !*** ./src/app/filter.pipe.ts ***!
  \********************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toLowerCase();
        return items.filter(function (it) {
            return it.name.toLowerCase().includes(searchText) || it.start.name.toLowerCase().includes(searchText) || it.end.name.toLowerCase().includes(searchText);
        });
    };
    FilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter'
        })
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/infrastructure/api.service.ts":
/*!***********************************************!*\
  !*** ./src/app/infrastructure/api.service.ts ***!
  \***********************************************/
/*! exports provided: MainService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainService", function() { return MainService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _jwt_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./jwt.service */ "./src/app/infrastructure/jwt.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * import services
 * */

var MainService = /** @class */ (function () {
    function MainService(http, jwtService) {
        this.http = http;
        this.jwtService = jwtService;
    }
    // Setting Headers for API Request
    MainService.prototype.setHeaders = function () {
        var headersConfig = {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        };
        if (this.jwtService.getToken()) {
            headersConfig['Authorization'] = "Bearer " + this.jwtService.getToken();
        }
        return new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"](headersConfig);
    };
    // Perform a GET Request
    MainService.prototype.get = function (path) {
        return this.http
            .get("" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url + path, { headers: this.setHeaders() })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            throw error.json();
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) { return res.json(); }));
    };
    // Perform a GET Request
    MainService.prototype.getFromOtherServer = function (path) {
        return this.http.get("" + path, { headers: this.setHeaders() }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            throw error.json();
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) { return res.json(); }));
    };
    // Perform a PUT Request
    MainService.prototype.put = function (path, body) {
        return this.http
            .put("" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url + path, JSON.stringify(this.filterInputs(body)), {
            headers: this.setHeaders()
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            throw error.json();
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) { return res.json(); }));
    };
    // Perform a PATCH Request
    MainService.prototype.patch = function (path, body) {
        return this.http
            .patch("" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url + path, JSON.stringify(this.filterInputs(body)), {
            headers: this.setHeaders()
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            throw error.json();
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) { return res.json(); }));
    };
    // Perform POST Request
    MainService.prototype.post = function (path, body) {
        return this.http
            .post("" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url + path, JSON.stringify(this.filterInputs(body)), {
            headers: this.setHeaders()
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            throw error.json();
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) { return res.json(); }));
    };
    // Perform Delete Request
    MainService.prototype.delete = function (path) {
        return this.http
            .delete("" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api_url + path, { headers: this.setHeaders() })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            throw error.json();
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) { return res.json(); }));
    };
    // filter the input values
    MainService.prototype.filterInputs = function (input) {
        var tempObj = {};
        Object.keys(input).forEach(function (key) {
            if (input[key] !== undefined &&
                input[key] !== null &&
                input[key] !== '') {
                tempObj[key] = input[key];
            }
        });
        return tempObj;
    };
    MainService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"], _jwt_service__WEBPACK_IMPORTED_MODULE_4__["JwtService"]])
    ], MainService);
    return MainService;
}());



/***/ }),

/***/ "./src/app/infrastructure/auth-guard.service.ts":
/*!******************************************************!*\
  !*** ./src/app/infrastructure/auth-guard.service.ts ***!
  \******************************************************/
/*! exports provided: AuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardService", function() { return AuthGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _jwt_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./jwt.service */ "./src/app/infrastructure/jwt.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * import services
 */

var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(router, _JwtService) {
        this.router = router;
        this._JwtService = _JwtService;
    }
    AuthGuardService.prototype.canActivate = function (route, state) {
        if (this._JwtService.getToken()) {
            // console.log(state.url);
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page
        this.router.navigateByUrl('/');
        return false;
    };
    AuthGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _jwt_service__WEBPACK_IMPORTED_MODULE_2__["JwtService"]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "./src/app/infrastructure/jwt.service.ts":
/*!***********************************************!*\
  !*** ./src/app/infrastructure/jwt.service.ts ***!
  \***********************************************/
/*! exports provided: JwtService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtService", function() { return JwtService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JwtService = /** @class */ (function () {
    function JwtService() {
    }
    JwtService.prototype.getToken = function () {
        return window.localStorage['token'];
    };
    JwtService.prototype.saveToken = function (token) {
        window.localStorage['token'] = token;
    };
    JwtService.prototype.destroyToken = function () {
        window.localStorage.clear();
    };
    JwtService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], JwtService);
    return JwtService;
}());



/***/ }),

/***/ "./src/app/modules/dashboard/md/md-table/md-table.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/modules/dashboard/md/md-table/md-table.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content table-responsive\">\r\n  <table class=\"table\">\r\n    <tbody>\r\n      <tr *ngFor=\"let row of data.dataRows\">\r\n        <td>\r\n          <div class=\"flag\">\r\n            <img src=\"./assets/img/flags/{{ row[0] }}.png\" alt=\"\" />\r\n          </div>\r\n        </td>\r\n        <td>{{ row[1] }}</td>\r\n        <td class=\"text-right\">{{ row[2] }}</td>\r\n        <td class=\"text-right\">{{ row[3] }}</td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/dashboard/md/md-table/md-table.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/modules/dashboard/md/md-table/md-table.component.ts ***!
  \*********************************************************************/
/*! exports provided: MdTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MdTableComponent", function() { return MdTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MdTableComponent = /** @class */ (function () {
    function MdTableComponent() {
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "subtitle", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "cardClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MdTableComponent.prototype, "data", void 0);
    MdTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-md-table',
            template: __webpack_require__(/*! ./md-table.component.html */ "./src/app/modules/dashboard/md/md-table/md-table.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], MdTableComponent);
    return MdTableComponent;
}());



/***/ }),

/***/ "./src/app/modules/dashboard/md/md.module.ts":
/*!***************************************************!*\
  !*** ./src/app/modules/dashboard/md/md.module.ts ***!
  \***************************************************/
/*! exports provided: NavItemType, MdModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavItemType", function() { return NavItemType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MdModule", function() { return MdModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _md_table_md_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./md-table/md-table.component */ "./src/app/modules/dashboard/md/md-table/md-table.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NavItemType;
(function (NavItemType) {
    NavItemType[NavItemType["Sidebar"] = 1] = "Sidebar";
    NavItemType[NavItemType["NavbarLeft"] = 2] = "NavbarLeft";
    NavItemType[NavItemType["NavbarRight"] = 3] = "NavbarRight"; // Right-aligned link on navbar in desktop mode, shown above sidebar items on collapsed sidebar in mobile mode
})(NavItemType || (NavItemType = {}));
var MdModule = /** @class */ (function () {
    function MdModule() {
    }
    MdModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            declarations: [_md_table_md_table_component__WEBPACK_IMPORTED_MODULE_3__["MdTableComponent"]],
            exports: [_md_table_md_table_component__WEBPACK_IMPORTED_MODULE_3__["MdTableComponent"]]
        })
    ], MdModule);
    return MdModule;
}());



/***/ }),

/***/ "./src/assets/js/particleJs.ts":
/*!*************************************!*\
  !*** ./src/assets/js/particleJs.ts ***!
  \*************************************/
/*! exports provided: particleStyle, particleProperties */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "particleStyle", function() { return particleStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "particleProperties", function() { return particleProperties; });
var particleStyle = {
    position: 'absolute',
    width: '100%',
    height: '100%',
    'z-index': -1,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    'background-color': '#232741',
    'background-size': 'cover',
    'background-repeat': 'no-repeat',
    'background-position': '50% 50%'
};
var particleProperties = {
//   particles: {
//     number: {
//       value: 218,
//       density: {
//         enable: true,
//         value_area: 789.1476416322727
//       }
//     },
//     color: {
//       value: '#ffffff'
//     },
//     shape: {
//       type: 'circle',
//       stroke: {
//         width: 0,
//         color: '#000000'
//       },
//       polygon: {
//         nb_sides: 4
//       },
//       image: {
//         src: 'img/github.svg',
//         width: 100,
//         height: 100
//       }
//     },
//     opacity: {
//       value: 1,
//       random: true,
//       anim: {
//         enable: true,
//         speed: 1.4617389821424212,
//         opacity_min: 0,
//         sync: false
//       }
//     },
//     size: {
//       value: 3,
//       random: true,
//       anim: {
//         enable: false,
//         speed: 4,
//         size_min: 0.3,
//         sync: false
//       }
//     },
//     line_linked: {
//       enable: false,
//       distance: 150,
//       color: '#ffffff',
//       opacity: 0.4,
//       width: 1
//     },
//     move: {
//       enable: true,
//       speed: 1,
//       direction: 'none',
//       random: true,
//       straight: false,
//       out_mode: 'out',
//       bounce: false,
//       attract: {
//         enable: false,
//         rotateX: 600,
//         rotateY: 600
//       }
//     }
//   },
//   interactivity: {
//     detect_on: 'canvas',
//     events: {
//       onhover: {
//         enable: true,
//         mode: 'bubble'
//       },
//       onclick: {
//         enable: true,
//         mode: 'repulse'
//       },
//       resize: true
//     },
//     modes: {
//       grab: {
//         distance: 400,
//         line_linked: {
//           opacity: 1
//         }
//       },
//       bubble: {
//         distance: 250,
//         size: 0,
//         duration: 2,
//         opacity: 0,
//         speed: 3
//       },
//       repulse: {
//         distance: 400,
//         duration: 0.4
//       },
//       push: {
//         particles_nb: 4
//       },
//       remove: {
//         particles_nb: 2
//       }
//     }
//   },
//   retina_detect: true
};


/***/ }),

/***/ "./src/assets/js/route-config.ts":
/*!***************************************!*\
  !*** ./src/assets/js/route-config.ts ***!
  \***************************************/
/*! exports provided: ADMIN_ROUTES, AGENT_ROUTES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADMIN_ROUTES", function() { return ADMIN_ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AGENT_ROUTES", function() { return AGENT_ROUTES; });
var ADMIN_ROUTES = [
    {
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    },
    {
        path: '/locations',
        title: 'Locations',
        type: 'link',
        icontype: 'dashboard'
    }, {
        path: '/train-stations',
        title: 'Train Stations',
        type: 'link',
        icontype: 'dashboard'
    },
    {
        path: '/trains',
        title: 'Trains',
        type: 'link',
        icontype: 'dashboard'
    },
    {
        path: '/time-tables',
        title: 'Time Tables',
        type: 'link',
        icontype: 'dashboard'
    },
    // {
    //   path: '/bookings',
    //   title: 'Booking',
    //   type: 'sub',
    //   icontype: 'shopping_cart',
    //   collapse: 'bookings',
    //   children: [
    //     { path: 'add-booking', title: 'Add', ab: 'A' },
    //     { path: 'booking-request', title: 'Requests', ab: 'R' }
    //   ]
    // },
    // {
    //   path: '/analytics',
    //   title: 'Analytics',
    //   type: 'sub',
    //   icontype: 'trending_up',
    //   collapse: 'Analytics',
    //   children: [{ path: 'predictions', title: 'Predictions', ab: 'PR' }]
    // },
    // {
    //   path: '/vehicle-management',
    //   title: 'Vehicle Management',
    //   type: 'sub',
    //   icontype: 'commute',
    //   collapse: 'Vehicle Management',
    //   children: [
    //     { path: 'listed-vehicles', title: 'View All', ab: 'VA' },
    //     { path: 'vehicle-register', title: 'Add', ab: 'A' },
    //     { path: 'pending-vehicles', title: 'Requests', ab: 'RE' }
    //   ]
    // },
    // {
    //   path: '/dealer-management',
    //   title: 'Dealer Management',
    //   type: 'sub',
    //   icontype: 'people',
    //   collapse: 'Dealer Management',
    //   children: [
    //     { path: 'info', title: 'View All', ab: 'VA' },
    //     { path: 'requests', title: 'Requests', ab: 'RE' }
    //   ]
    // },
    // {
    //   path: '/sales',
    //   title: 'Branch Management',
    //   type: 'sub',
    //   icontype: 'business',
    //   collapse: 'Branch Management',
    //   children: [{ path: 'view-sales', title: 'View All', ab: 'VA' }]
    // },
    // {
    //   path: '/notifications',
    //   title: 'Notifications',
    //   type: 'link',
    //   icontype: 'add_alert'
    // },
    {
        path: '/example',
        title: 'Configurations',
        type: 'link',
        icontype: 'settings'
    }
];
var AGENT_ROUTES = [
    {
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    },
    {
        path: '/bookings',
        title: 'Booking',
        type: 'sub',
        icontype: 'shopping_cart',
        collapse: 'bookings',
        children: [
            { path: 'add-booking', title: 'Add', ab: 'A' },
            { path: 'booking-request', title: 'Requests', ab: 'R' }
        ]
    },
    {
        path: '/analytics',
        title: 'Analytics',
        type: 'sub',
        icontype: 'trending_up',
        collapse: 'Analytics',
        children: [{ path: 'predictions', title: 'Predictions', ab: 'PR' }]
    },
    {
        path: '/vehicle-management',
        title: 'Vehicle Management',
        type: 'sub',
        icontype: 'commute',
        collapse: 'Vehicle Management',
        children: [
            { path: 'listed-vehicles', title: 'View All', ab: 'VA' },
            { path: 'vehicle-register', title: 'Add', ab: 'A' }
        ]
    },
    {
        path: '/sales',
        title: 'Sales Management',
        type: 'sub',
        icontype: 'business',
        collapse: 'Branch Management',
        children: [
            { path: 'view-sales', title: 'View All', ab: 'VA' },
            { path: 'add-sales', title: 'Add', ab: 'A' }
        ]
    }
];


/***/ }),

/***/ "./src/assets/js/user-configs.ts":
/*!***************************************!*\
  !*** ./src/assets/js/user-configs.ts ***!
  \***************************************/
/*! exports provided: USER_ROLE */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_ROLE", function() { return USER_ROLE; });
var USER_ROLE = {
    ADMIN: 'admin',
    AGENT: 'agent',
    CUSTOMER: 'customer'
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: false,
    api_url: 'http://localhost:3000/',
    imageUploadPath: 'http://localhost:3200/uploads',
    apiKey: 'AIzaSyACSamHQMZZx7rMiRuj55d4Gvsfx3JfQH8',
    publicSiteUrl: 'http://liveTrains.com'
};
// 178.128.126.7 = production


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Live_train\research-2019-admin-back-end\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map