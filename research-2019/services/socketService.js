// Import required libraries
const socket = require('socket.io');
const commonService = require('./commonServices');
const TimeTableService = require('../src/time-table/timeTableService');
const googleApiService = require('../services/google-api-service');
const trainService = require('../src/train/trainService');
// Define global variable
let io;

// Initialize the socket
async function listen(app) {
    io = socket.listen(app);
    io.on('connect',async socket => {
var trainDataset =await trainService.findOne("5d594e03b789ed3ca4202605");
console.log(trainDataset.selectedPoints);
        console.log('user connected');
        socket.on('disconnect', function () {
            console.log('user disconnected');
        });
        socket.on('realtime-data', function (socket) {
            console.log(socket);
          });
          socket.on('validate-data',async function (socket) {
            
           var res = await googleApiService.checkPointIsOnThePath(socket,trainDataset.selectedPoints);
           console.log(res);
           emitValidateResData(res);
          });
          
    });
    return io;
}
async function readMainValveData() {
    io.on('realtime-data', function (socket) {
        console.log("user saved message");
        console.log(socket);
      });
}
//broadcast data which added to the db
async function emitMainValveData(data) {
    try {
       let res= await TimeTableService.getAll();
        io.emit('realTimeTable', res);
    } catch (error) {
        io.emit('realTimeTable', data);
    }
 
}
async function emitValidateResData(data) {
    try {
        io.emit('validateRes', data);
    } catch (error) {
        io.emit('validateRes', data);
    }
 
}

//export functions
module.exports = {
    listen,
    emitValidateResData,
    emitMainValveData,
    readMainValveData,
    io
};