const googleApiService = require('./googleApiService');
const googleApiCoreService = require('../../services/google-api-service');
const trainService = require('../train/trainService');
const commonService = require('../../services/commonServices');
const response = require('../../services/responseService');

/**
 * get all valves
 */
module.exports.getDistance = async (req, res) => {
    try {
        console.log("***********contraller");
        let details = JSON.parse(JSON.stringify(req.body));
        let items = await googleApiService.getDistance(details);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get unregistered valves
 */
module.exports.getNearByPlaces = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        let items = await googleApiService.getNearByPlaces(details);
        console.log(items);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get unregistered valves
 */
module.exports.getDataWithArrivelTime = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        let items = await googleApiService.getDataWithArrivelTime(details);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get speed
 */
module.exports.getSpeed = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        let items = await googleApiService.getSpeed(details);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
    /**
 * get unregistered valves
 */
module.exports.validatePointAndPath = async (req, res) => {
    try {
        console.log(req.body.id);
        let trainDetails = await trainService.findOne(req.body.id);
        // console.log(trainDetails);
        if(trainDetails.selectedPoints){
            console.log(req.body.point);
         let status =    await googleApiCoreService.checkPointIsOnThePath(req.body.point,trainDetails.selectedPoints);
            if(status){
                response.successWithData({"validateStatus":true}, res);
            }else{
                response.successWithData({"validateStatus":false}, res);
            }
        }else{
            response.customError("train or selected points not found", res);
        }
    } catch (error) {
        response.customError('' + error, res)
    }
}

